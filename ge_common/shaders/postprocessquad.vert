out vec2 coords;

const vec2 quadVertices[4] = { vec2(-1.0f, -1.0f), vec2(1.0f, -1.0f), vec2(-1.0f, 1.0f), vec2(1.0f, 1.0f) };
void main() {
    coords = (quadVertices[gl_VertexID] + 1.0f) / 2.0f;
    gl_Position = vec4(quadVertices[gl_VertexID], 0.0f, 1.0f);
}