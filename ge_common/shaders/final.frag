layout (binding = 0) uniform sampler2D colour;

in vec2 coords;
out vec4 fragColour;

void main() {
    fragColour = vec4(pow(texture(colour, coords).rgb, vec3(1.0f / 2.2f)), 1.0f);
}