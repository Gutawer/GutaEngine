layout (binding = 0) uniform sampler2D position;
layout (binding = 1) uniform sampler2D normal;
layout (binding = 2) uniform sampler2D diffuse;
layout (binding = 3) uniform sampler2D specular;
layout (binding = 4) uniform sampler2D ao;
layout (binding = 5) uniform sampler2D emission;

uniform vec3 ambientLightColour;

in vec2 coords;

out vec4 fragColour;

void main() {
    vec3 aoColour = texture(ao, coords).rgb;
    vec3 diffColour = texture(diffuse, coords).rgb;
    vec3 emissionColour = texture(emission, coords).rgb;

    vec3 ambient = aoColour * diffColour * ambientLightColour;
    fragColour = vec4(ambient + diffColour * emissionColour, 1.0f);
}
