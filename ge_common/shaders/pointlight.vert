layout(location = 0) in vec3 vertexPosition;

uniform mat4 view, proj;
uniform float lightRange;
uniform vec3 lightPos;

void main() {
    gl_Position = proj * view * (lightRange * vec4(vertexPosition, 0.0f) + vec4(lightPos, 1.0f));
}