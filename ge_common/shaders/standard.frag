in vec2 texCoords;
in vec3 viewDirTangent;
in vec3 lightDirTangent;
in vec3 positionWorld;

in mat3 tbn;

layout (location = 0) out vec4 position;
layout (location = 1) out vec4 normal;
layout (location = 2) out vec4 diffuse;
layout (location = 3) out vec4 specular;
layout (location = 4) out vec4 ao;
layout (location = 5) out vec4 emission;
layout (location = 6) out vec4 fragColour;

uniform float timer, lightIntensity, lightRangeSquared;
uniform mat4 model, view, proj;
uniform vec3 lightPos, lightColour;

layout (binding = 0) uniform sampler2D textureDiff;
layout (binding = 1) uniform sampler2D textureSpec;
layout (binding = 2) uniform sampler2D textureAO;
layout (binding = 3) uniform sampler2D textureEmission;
layout (binding = 4) uniform sampler2D textureNormal;

void main() {
    vec4 diffColour     = texture(textureDiff    , texCoords);
    vec4 specColour     = texture(textureSpec    , texCoords);
    vec4 aoColour       = texture(textureAO      , texCoords);
    vec4 emissionColour = texture(textureEmission, texCoords);
    
    vec3 normalTangent = texture(textureNormal, texCoords).rgb;
    normalTangent = normalize(normalTangent * 2.0f - 1.0f);
    vec3 mappedNormal = normalize(tbn * normalTangent);

    position = vec4(positionWorld, 1.0f);
    normal = vec4(mappedNormal, 0.0f);
    diffuse = vec4(diffColour.rgb, 1.0f);
    specular = vec4(specColour.rgb, 1.0f);
    ao = vec4(aoColour.rgb, 1.0f);
    emission = vec4(emissionColour.rgb, 1.0f);
    fragColour = vec4(0.0f);
}
