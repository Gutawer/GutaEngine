layout (binding = 0) uniform sampler2D position;
layout (binding = 1) uniform sampler2D normal;
layout (binding = 2) uniform sampler2D diffuse;
layout (binding = 3) uniform sampler2D specular;
layout (binding = 4) uniform sampler2D ao;
layout (binding = 5) uniform sampler2D emission;

uniform vec3 lightColour, lightPos, viewPos;
uniform float lightIntensity, lightRange;

out vec4 fragColour;

void main() {
    vec2 texCoord = gl_FragCoord.xy / textureSize(position, 0);
    vec4 pos = texture(position, texCoord);
    if (pos.w > 0.5f) {
        vec3 directionToLightWorld = normalize(lightPos - pos.xyz);
        vec3 normalWorld = texture(normal, texCoord).xyz;
        fragColour = vec4(directionToLightWorld / 100.0f, 1.0f);
        float dotProd = dot(directionToLightWorld, normalWorld);
        dotProd = max(dotProd, 0.0f);
        vec3 diffuse = texture(diffuse, texCoord).rgb * lightColour * dotProd;

        vec3 reflection = reflect(-directionToLightWorld, normalWorld);
        float dotProdSpecular = dot(reflection, normalize(viewPos - pos.xyz));
        dotProdSpecular = max(dotProdSpecular, 0.0f);
        float specularFactor = pow(dotProdSpecular, 100.0f);
        vec3 specular = texture(specular, texCoord).rgb * lightColour * specularFactor;

        vec3 diff = lightPos - pos.xyz;
        float distanceSquared = dot(diff, diff);
        float lightRangeSquared = lightRange * lightRange;
        float attenuation = lightIntensity / (1.0f + distanceSquared);
        attenuation *= smoothstep(0.0f, 1.0f, clamp(5 * (lightRangeSquared - distanceSquared) / lightRangeSquared, 0.0f, 1.0f));
        fragColour = vec4(attenuation * (diffuse + specular), 1.0f);
    }
}