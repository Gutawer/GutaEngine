layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec2 vertexTexCoords;
layout(location = 3) in vec4 vertexTangent;

out vec2 texCoords;
out vec3 viewDirTangent;
out vec3 lightDirTangent;
out vec3 positionWorld;

out mat3 tbn;

uniform float timer;
uniform mat4 model, view, modelView, proj;
uniform mat3 normal;
uniform vec3 ambientLightColour, lightPos, lightColour, camPos;

void main() {
    positionWorld = vec3(model * vec4(vertexPosition, 1.0f));
    texCoords = vertexTexCoords;

    vec3 vertexPositionWorld = vec3(model * vec4(vertexPosition, 1.0f));

    vec3 viewDirWorld = normalize(camPos - vertexPositionWorld);

    vec3 lightDirWorld = normalize(lightPos - vertexPositionWorld);

    vec3 bitangent = cross(vertexNormal, vertexTangent.xyz) * vertexTangent.w;

    vec3 tangentWorld = normalize(vec3(model * vec4(vertexTangent.xyz, 0.0f)));
    vec3 bitangentWorld = normalize(vec3(model * vec4(bitangent, 0.0f)));
    vec3 normalWorld = normalize(vec3(normal * vertexNormal));

    tbn[0] = tangentWorld;
    tbn[1] = bitangentWorld;
    tbn[2] = normalWorld;

    /*viewDirTangent = vec3(
        dot(tangentWorld, viewDirWorld),
        dot(bitangentWorld, viewDirWorld),
        dot(normalWorld, viewDirWorld)
    );

    lightDirTangent = vec3(
        dot(tangentWorld, lightDirWorld),
        dot(bitangentWorld, lightDirWorld),
        dot(normalWorld, lightDirWorld)
    );*/

    gl_Position = proj * modelView * vec4(vertexPosition, 1.0f);
}
