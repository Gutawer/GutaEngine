#include "world.hpp"
#include "actor.hpp"
#include "player.hpp"
#include "graphics/light.hpp"
#include "graphics/render_mesh.hpp"

void World::beginWorld() {
	player = spawnActor<PlayerPawn>();

	testLights[0] = spawnActor<DynamicLight>();
	testLights[0]->getLightComponent()->lightColour = Colour(1.0f, 1.0f, 0.0f);
	testLights[0]->transform.translation = Vector3F(15.0f, 20.0f, 72.0f);
	testLights[0]->getLightComponent()->intensity = 128.0f;
	testLights[0]->getLightComponent()->range = 48.0f;

	testLights[1] = spawnActor<DynamicLight>();
	testLights[1]->getLightComponent()->lightColour = Colour(0.0f, 1.0f, 1.0f);
	testLights[1]->transform.translation = Vector3F(15.0f, -20.0f, 72.0f);
	testLights[1]->getLightComponent()->intensity = 128.0f;
	testLights[1]->getLightComponent()->range = 48.0f;

	testLights[2] = spawnActor<DynamicLight>();
	testLights[2]->getLightComponent()->lightColour = Colour(1.0f, 0.0f, 1.0f);
	testLights[2]->transform.translation = Vector3F(-15.0f, 0.0f, 72.0f);
	testLights[2]->getLightComponent()->intensity = 128.0f;
	testLights[2]->getLightComponent()->range = 48.0f;

	modelTest = spawnActor<Actor>();
	modelTest->addComponent<RenderMeshComponent>(RenderMesh::getMesh(filesystem::ResourceLocation("models/test.obj")));
}

void World::tick() {
	actors.unique();
	for (auto& actor : actors) actor->tick();
	//testLight->transform.translation.x = std::cos(time / 240.0f) * 112.0f;
	//testLight->transform.translation.y = std::sin(time / 240.0f) * 112.0f;
	//testLight->transform.translation.z = std::sin(time / 120.0f) * 72.0f;
	//testLight->getLightComponent()->intensity = 512.0f;

	//modelTest->transform.translation = Vector3F(0.0f, 0.0f, 62.0f);
	//modelTest->transform.orientation *= QuaternionF::yawPitchRoll(0.01f, 0.0f, 0.0f);
	//modelTest->transform.scale = Vector3F(1.0f, 1.0f, 0.01f);

	time++;
}

int World::getTime() {
	return time;
}
