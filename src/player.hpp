#pragma once
#include "actor.hpp"
#include "input/input.hpp"
#include "camera.hpp"
#include "maths/vector/vector.hpp"

class PlayerPawn : public Actor {
public:
	PlayerPawn(std::shared_ptr<World> world) : Actor(world) {}

	void beginPlay() override;

	virtual void setupInputComponent();

	void moveForward(float axisAmount);
	void moveSideways(float axisAmount);
	void moveUpwards(float axisAmount);

	void changeYaw(float axisAmount);
	void changePitch(float axisAmount);
	void changeRoll(float axisAmount);

	void resetPosOrientation();

	void tick() override;

	Vector3F moveVec;

	float yaw = 0.0f;
	float pitch = 0.0f;
	float roll = 0.0f;

	bool resettingPosOrientation = false;
	Vector3F resetPos;
	QuaternionF resetOrientation;
	float resettingPosOrientationProgress = 0.0f;

	input::InputComponent* getInputComponent();
	CameraComponent* getCameraComponent();
protected:
	input::InputComponent* inputComponent = nullptr;
	CameraComponent* cameraComponent = nullptr;
};