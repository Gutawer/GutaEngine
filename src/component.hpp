#pragma once
#include <memory>

class Actor;

class ActorComponent {
	friend class Actor;
public:
	ActorComponent(Actor* owner) : owner(owner) {}

	Actor* getOwner() const;

	void destroy();

	virtual ~ActorComponent() = 0;
protected:
	virtual void beginPlay() {}
	virtual void onDestroy() {}
private:
	Actor* owner;
};