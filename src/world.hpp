#pragma once
#include "maths/vector/vector.hpp"
#include <memory>
#include <list>
#include <set>

class PlayerPawn;
class Actor;
class RenderLightComponent;
class DynamicLight;

class World : public std::enable_shared_from_this<World> {
public:
	World() {}

	void beginWorld();

	std::list<std::unique_ptr<Actor>> actors;

	std::set<RenderLightComponent*> lights;

	// very temporary, replaceme
	PlayerPawn* player;
	
	Actor* modelTest;

	DynamicLight* testLights[100];

	template <typename T>
	T* spawnActor(Vector3F pos = Vector3F());

	void tick();

	int getTime();
private:
	int time = 0;
};

template<typename T>
T* World::spawnActor(Vector3F pos) {
	std::unique_ptr<T> ptr = std::make_unique<T>(shared_from_this());
	T* ret = ptr.get();
	actors.push_back(std::move(ptr));
	ret->transform.translation = pos;
	ret->beginPlay();
	return ret;
}
