#include "game.hpp"
#include <memory>
#include "fmt/format.h"

int main(int argc, const char* argv[]) {
	try {
		Game game = Game(argc, argv);
		game.run();
	}
	catch (const GameException& e) {
		return 1;
	}
	return 0;
}
