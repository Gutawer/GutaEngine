#include "window.hpp"
#include "player.hpp"

namespace window {
	void WindowManager::setInput(Game* game, std::weak_ptr<input::InputMapper> mapper) {
		windowGame = game;
		windowInputMapper = mapper;
	}

	Vector2I WindowManager::getWindowSize() {
		return windowSize;
	}

	Vector2I WindowManager::getFramebufferSize() {
		return framebufferSize;
	}
}