#pragma once
#include "maths/vector/vector.hpp"
#include "input/input.hpp"
#include <stdexcept>
#include <string>
#include <memory>

class Game;
class PlayerPawn;

namespace window {
	class WindowManagerException : public std::runtime_error {
	public:
		WindowManagerException(std::string msg) : std::runtime_error(msg) {}
	};

	class WindowManager {
		friend class Game;
	protected:
		Vector2I windowSize;
		Vector2I framebufferSize;

		Game* windowGame;
		std::weak_ptr<input::InputMapper> windowInputMapper;
	public:
		void setInput(Game* game, std::weak_ptr<input::InputMapper> mapper);

		Vector2I getWindowSize();
		Vector2I getFramebufferSize();

		virtual void update(Game& game) = 0;
		virtual void changeWindowTitle(std::string title) = 0;

		virtual void swapBuffers() = 0;
		virtual bool shouldClose() = 0;
	};
}