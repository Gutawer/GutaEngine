#include "window_glfw.hpp"
#include "game.hpp"
#include <iostream>

namespace window {
	void GLFWWindowManager::glfwErrorCallback(int error, const char* description) {
		filesystem::errorLog(filesystem::LogType::Window, "GLFW error: code {}, message \"{}\".\n", error, description);
	}

	void GLFWWindowManager::glfwWindowSizeCallback(GLFWwindow* window, int width, int height) {
		GLFWWindowManager* manager = static_cast<GLFWWindowManager*>(glfwGetWindowUserPointer(window));
		manager->windowSize.x = width; manager->windowSize.y = height;
		filesystem::log(filesystem::LogType::Window, "Window size changed to {}.\n", manager->windowSize);
	}

	void GLFWWindowManager::glfwFramebufferSizeCallback(GLFWwindow* window, int width, int height) {
		GLFWWindowManager* manager = static_cast<GLFWWindowManager*>(glfwGetWindowUserPointer(window));
		manager->framebufferSize.x = width; manager->framebufferSize.y = height;
		filesystem::log(filesystem::LogType::Window, "Framebuffer size changed to {}.\n", manager->framebufferSize);
	}

	void GLFWWindowManager::glfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
		GLFWWindowManager* manager = static_cast<GLFWWindowManager*>(glfwGetWindowUserPointer(window));
		if (!manager->windowInputMapper.expired() && manager->windowGame->getWorld() != nullptr && (action == GLFW_PRESS || action == GLFW_RELEASE)) {
			input::KeyCode code = glfwKeyEnumToKeyCode(key);
			auto sharedInputMapper = manager->windowInputMapper.lock();
			PlayerPawn* player = manager->windowGame->getWorld()->player;
			input::InputType type = sharedInputMapper->getKeyCodeInputType(code);

			if (type == input::InputType::None) return;
			else if (type == input::InputType::Action) {
				input::ActionType actionType = (action == GLFW_PRESS) ? input::ActionType::Pressed : input::ActionType::Released;
				std::string actionName = sharedInputMapper->getKeyCodeAction(code);
				player->getInputComponent()->incrementActionCallCount(actionName, actionType);
			}
			else if (type == input::InputType::Axis) {
				auto axisData = sharedInputMapper->getKeyCodeAxis(code);
				std::string axisName = std::get<0>(axisData);
				float axisAmount = std::get<1>(axisData);
				if      (action == GLFW_PRESS  ) player->getInputComponent()->setLatestAxisValue(axisName, code, axisAmount);
				else if (action == GLFW_RELEASE) player->getInputComponent()->setLatestAxisValue(axisName, code, 0.0f);
			}
		}
	}

	input::KeyCode GLFWWindowManager::glfwKeyEnumToKeyCode(int glfwKey) {
		auto iter = glfwKeyToCodeMap.find(glfwKey);
		if (iter == glfwKeyToCodeMap.end()) return input::KeyCode::Unknown;
		else                                return iter->second;
	}

	GLFWWindowManager::GLFWWindowManager() {
		filesystem::restartLog(filesystem::LogType::Window);
		filesystem::log(filesystem::LogType::Window, "Starting GLFW:\nGLFW version string: \"{}\"\n", glfwGetVersionString());
		glfwSetErrorCallback(glfwErrorCallback);

		if (!glfwInit()) {
			std::string msg = "Error: could not start GLFW3.\n";
			filesystem::errorLog(filesystem::LogType::Window, msg);
			throw WindowManagerException(msg);
		}

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		glfwWindowHint(GLFW_SAMPLES, 4);

		window = glfwCreateWindow(1280, 720, "OpenGL - ", NULL, NULL);
		if (window == nullptr) {
			std::string msg = "Error: could not create GLFW3 window.\n";
			filesystem::errorLog(filesystem::LogType::Window, msg);
			glfwTerminate();
			throw WindowManagerException(msg);
		}

		glfwMakeContextCurrent(window);

		glfwSwapInterval(0);

		glfwSetWindowUserPointer(window, this);

		glfwGetWindowSize(window, &windowSize.x, &windowSize.y);
		glfwGetFramebufferSize(window, &framebufferSize.x, &framebufferSize.y);

		glfwSetWindowSizeCallback(window, glfwWindowSizeCallback);
		glfwSetFramebufferSizeCallback(window, glfwFramebufferSizeCallback);

		glfwSetKeyCallback(window, glfwKeyCallback);
	}

	GLFWWindowManager::~GLFWWindowManager() {
		glfwDestroyWindow(window);
	}

	void GLFWWindowManager::update(Game& game) {
		glfwMakeContextCurrent(window);

		glfwPollEvents();
		if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_ESCAPE)) {
			glfwSetWindowShouldClose(window, true);
		}
	}

	void GLFWWindowManager::changeWindowTitle(std::string title) {
		glfwSetWindowTitle(window, title.c_str());
	}

	void GLFWWindowManager::swapBuffers() {
		glfwSwapBuffers(window);
	}

	bool GLFWWindowManager::shouldClose() {
		return glfwWindowShouldClose(window);
	}
}