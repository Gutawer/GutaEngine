#pragma once
#include "graphics/graphics.hpp"
#include "window.hpp"
#include "input/input.hpp"
#include "file.hpp"
#include <unordered_map>

namespace window {
	class GLFWWindowManager : public WindowManager {
	private:
		GLFWwindow* window;

		static void glfwErrorCallback(int error, const char* description);
		static void glfwWindowSizeCallback(GLFWwindow* window, int width, int height);
		static void glfwFramebufferSizeCallback(GLFWwindow* window, int width, int height);
		static void glfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);

		static const std::unordered_map<int, input::KeyCode> glfwKeyToCodeMap;

		static input::KeyCode glfwKeyEnumToKeyCode(int glfwKey);
	public:
		GLFWWindowManager();
		~GLFWWindowManager();

		void update(Game& game) override;
		void changeWindowTitle(std::string title) override;

		void swapBuffers() override;
		bool shouldClose() override;
	};
}