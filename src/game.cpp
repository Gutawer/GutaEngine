#include "game.hpp"
#include "window/window_glfw.hpp"
#include "graphics/graphics_gl.hpp"
#include <chrono>
#include <iostream>

Game::Game(int argc, const char* argv[]) : args(std::vector<std::string>(argv + 1, argv + argc)) {
	try {
		window = std::make_shared<window::GLFWWindowManager>();
	}
	catch (const window::WindowManagerException& e) {
		throw GameException(e.what());
	}

	try {
		renderer = std::make_shared<graphics::OpenGLRenderer>(window);
	}
	catch (const graphics::RendererException& e) {
		throw GameException(e.what());
	}

	world = std::make_shared<World>();
	world->beginWorld();

	inputMapper = std::make_shared<input::InputMapper>();

	window->setInput(this, inputMapper);

	inputMapper->addAxis(input::KeyCode::W, "moveForward",  1.0);
	inputMapper->addAxis(input::KeyCode::S, "moveForward", -1.0);

	inputMapper->addAxis(input::KeyCode::D, "moveSideways",  1.0);
	inputMapper->addAxis(input::KeyCode::A, "moveSideways", -1.0);

	inputMapper->addAxis(input::KeyCode::Space,     "moveUpwards",  1.0);
	inputMapper->addAxis(input::KeyCode::LeftShift, "moveUpwards", -1.0);

	inputMapper->addAxis(input::KeyCode::Left,  "changeYaw",  0.01f);
	inputMapper->addAxis(input::KeyCode::Right, "changeYaw", -0.01f);

	inputMapper->addAxis(input::KeyCode::Up,   "changePitch",  0.01f);
	inputMapper->addAxis(input::KeyCode::Down, "changePitch", -0.01f);

	inputMapper->addAxis(input::KeyCode::E, "changeRoll",  0.01f);
	inputMapper->addAxis(input::KeyCode::Q, "changeRoll", -0.01f);

	inputMapper->addAction(input::KeyCode::R, "resetPosOrientation");
}

unsigned int Game::getGameTic() const {
	return gameTic;
}

std::shared_ptr<World> Game::getWorld() {
	return world;
}

std::shared_ptr<World> Game::getWorld() const {
	return world;
}

void Game::run() {
	const float dt = 1000 / 60.0f;
	auto previousTime = std::chrono::high_resolution_clock::now();
	auto previousTimeFPS = previousTime;
	int frameCount = 0;
	float timeAccumulator = 0.0f;

	while (!window->shouldClose()) {
		auto currentTime = std::chrono::high_resolution_clock::now();
		auto currentTimeFPS = currentTime;
		float elapsedTimeFPS = std::chrono::duration_cast<std::chrono::microseconds>(currentTimeFPS - previousTimeFPS).count() / 1000.0f;
		float elapsedTime = std::chrono::duration_cast<std::chrono::microseconds>(currentTime - previousTime).count() / 1000.0f;
		previousTime = currentTime;
		
		timeAccumulator += elapsedTime;

		while (timeAccumulator > dt) {
			auto playerInput = world->player->getInputComponent();
			if (playerInput != nullptr) {
				for (const auto& e : playerInput->latestAxisAccumulatorValues) {
					std::string axisName = std::get<0>(e);
					float axisVal = 0.0f;
					const auto& vals = std::get<1>(e);
					for (const auto& v : vals) {
						axisVal += std::get<1>(v);
					}
					playerInput->axisValues[axisName] = axisVal;
					playerInput->callAxis(axisName, axisVal);
				}
				for (const auto& e : playerInput->actionCallCount) {
					std::string actionName = std::get<0>(e);
					const auto& vals = std::get<1>(e);
					for (const auto& v : vals) {
						for (unsigned int i = 0; i < std::get<1>(v); i++) playerInput->callAction(actionName, std::get<0>(v));
					}
				}
				playerInput->actionCallCount.clear();
			}

			world->tick();

			timeAccumulator -= dt;
			gameTic += 1;
		}

		if (elapsedTimeFPS > 250) {
			previousTimeFPS = currentTimeFPS;
			float fps = (1000 * frameCount) / elapsedTimeFPS;
			float frameTime = elapsedTimeFPS / frameCount;
			window->changeWindowTitle("OpenGL - " + std::to_string(fps) + "fps, " + std::to_string(frameTime) + "ms\n");
			frameCount = 0;
		}
		frameCount++;

		renderer->render(*this, timeAccumulator / dt);
		window->update(*this);
	}
}