#include "input/input.hpp"
#include <iostream>

namespace input {
	void InputComponent::setLatestAxisValue(std::string axis, input::KeyCode code, float value) {
		latestAxisAccumulatorValues[axis][code] = value;
	}

	void InputComponent::incrementActionCallCount(std::string action, ActionType type) {
		actionCallCount[action][type]++;
	}

	void InputComponent::bindAxis(std::string axisName, std::function<void(float axisAmount)> func) {
		axisBindings[axisName] = func;
	}

	void InputComponent::bindAction(std::string actionName, ActionType type, std::function<void(void)> func) {
		actionBindings[actionName][type] = func;
	}

	void InputComponent::callAxis(std::string axisName, float axisAmount) {
		auto iter = axisBindings.find(axisName);
		if (iter == axisBindings.end()) return;
		else iter->second(axisAmount);
	}

	void InputComponent::callAction(std::string actionName, ActionType type) {
		auto bindingIter = actionBindings.find(actionName);
		if (bindingIter == actionBindings.end()) return;
		else {
			auto typeIter = bindingIter->second.find(type);
			if (typeIter == bindingIter->second.end()) return;
			else typeIter->second();
		}
	}

	float InputComponent::getAxisValue(std::string axisName) {
		return axisValues[axisName];
	}

	InputType InputMapper::getKeyCodeInputType(KeyCode code) {
		auto iter = keyCodeTypeMap.find(code);

		if (iter == keyCodeTypeMap.end()) return InputType::None;
		else                              return iter->second;
	}

	std::string InputMapper::getKeyCodeAction(KeyCode code) {
		auto iter = keyCodeActionMap.find(code);

		if (iter == keyCodeActionMap.end()) return "";
		else                                return iter->second;
	}

	std::pair<std::string, float> InputMapper::getKeyCodeAxis(KeyCode code) {
		auto iter = keyCodeAxisMap.find(code);

		if (iter == keyCodeAxisMap.end()) return { "", 0.0f };
		else                              return iter->second;
	}

	void InputMapper::addAxis(KeyCode code, std::string axisName, float axisAmount) {
		if (axisName == "") return;
		keyCodeTypeMap[code] = InputType::Axis;
		keyCodeAxisMap[code] = { axisName, axisAmount };
	}

	void InputMapper::addAction(KeyCode code, std::string actionName) {
		if (actionName == "") return;
		keyCodeTypeMap[code] = InputType::Action;
		keyCodeActionMap[code] = actionName;
	}
}