#pragma once
#include "component.hpp"
#include "input/keys.hpp"
#include <unordered_map>
#include <string>
#include <functional>
#include <tuple>
#include <queue>

class Game;

namespace input {
	/// Enum for action types (e.g. pressing a key, releasing).
	enum class ActionType {
		Pressed,
		Released
	};

	/// A component for binding inputs to actor functions.
	class InputComponent : public ActorComponent {
		friend class ::Game;
	public:
		InputComponent(Actor* owner) : ActorComponent(owner) {}

		///
		/// \brief Sets the latest axis value of a key on an axis.
		///
		/// Sets the latest axis value of a key on an axis.
		///
		/// \param axis  The axis to set to
		/// \param code  The KeyCode to set to on the axis
		/// \param value The value to set for the specified code on the specified axis
		///
		void setLatestAxisValue(std::string axis, input::KeyCode code, float value);

		///
		/// \brief Increments the call count for an action and type.
		///
		/// Increments the call count for an action and type.
		///
		/// \param action The action to increment
		/// \param type   The type of action to increment
		///
		void incrementActionCallCount(std::string action, ActionType type);

		///
		/// \brief Binds a function to an axis.
		///
		/// Binds a function to an axis to be called by the input code every tick.
		///
		/// \param axisName The name of the axis to bind to.
		/// \param func     The function to bind to axisName
		///
		void bindAxis(std::string axisName, std::function<void(float axisAmount)> func);

		///
		/// \brief Binds a function to an action.
		///
		/// Binds a function to an action to be called by the input code at the start of each tick.
		///
		/// \param actionName The name of the action to bind to.
		/// \param func       The function to bind to actionName
		///
		void bindAction(std::string actionName, ActionType type, std::function<void(void)> func);

		///
		/// \brief Gets the current value for an axis.
		///
		/// Gets the current value for an axis.
		///
		/// \param axisName The axis to fetch from
		///
		/// \return The value for axisName
		///
		float getAxisValue(std::string axisName);
	private:
		///
		/// \brief Calls the function bound to an axis.
		///
		/// Calls the function bound to an axis.
		///
		/// \param axisName   The axis to call the function from
		/// \param axisAmount The value to pass to the axis' function
		///
		void callAxis(std::string axisName, float axisAmount);

		///
		/// \brief Calls the function bound to an action.
		///
		/// Calls the function bound to an action.
		///
		/// \param actionName The action to call the function from
		/// \param type       The type of action to call from (e.g. ActionType::Pressed)
		///
		void callAction(std::string actionName, ActionType type);

		std::unordered_map<std::string, float> axisValues;
		std::unordered_map<std::string, std::function<void(float axisAmount)>> axisBindings;
		std::unordered_map<std::string, std::unordered_map<ActionType, std::function<void(void)>>> actionBindings;

		std::unordered_map<std::string, std::unordered_map<KeyCode, float>> latestAxisAccumulatorValues;
		std::unordered_map<std::string, std::unordered_map<ActionType, unsigned int>> actionCallCount;
	};

	/// Enum for input types from the key mapper.
	enum class InputType {
		None = -1,
		Action,
		Axis
	};

	/// Class for mapping keycodes to inputs usable by the game.
	class InputMapper {
	public:
		///
		/// \brief Gets the input type associated with a key code.
		///
		/// Gets the input type associated with a key code.
		///
		/// \param code The code to get input type from
		///
		/// \return The type of input code is bound to (returns InputType::None if code isn't bound).
		///
		InputType getKeyCodeInputType(KeyCode code);

		///
		/// \brief Gets the action name for a key code.
		///
		/// Gets the action name for a key code.
		///
		/// \param code The code to get action name from
		///
		/// \return The axis name ("" if no action was found for this code)
		///
		std::string getKeyCodeAction(KeyCode code);

		///
		/// \brief Gets the axis name and amount for a key code.
		///
		/// Gets the axis name and amount for a key code.
		///
		/// \param code The code to get axis name and amount from
		///
		/// \return A pair containing a string for axis name, and a float for axis amount for this keycode  ("" and 0.0f respectively if no axis was found for this code)
		///
		std::pair<std::string, float> getKeyCodeAxis(KeyCode code);

		///
		/// \brief Associates a key code with an axis and amount.
		///
		/// Associates a key code with an axis and floating-point amount to apply to this axis from this button.
		///
		/// \param code       The key code to associate this axis with
		/// \param axisName   The name of the axis to associate with
		/// \param axisAmount The amount to apply to this axis when this key is pressed
		///
		void addAxis(KeyCode code, std::string axisName, float axisAmount);

		///
		/// \brief Associates a key code with an action.
		///
		/// Associates a key code with an action.
		///
		/// \param code       The key code to associate this action with
		/// \param actionName The name of the action to associate with
		///
		void addAction(KeyCode code, std::string actionName);
	private:
		std::unordered_map<KeyCode, InputType> keyCodeTypeMap;
		std::unordered_map<KeyCode, std::string> keyCodeActionMap;
		std::unordered_map<KeyCode, std::pair<std::string, float>> keyCodeAxisMap;
	};
}