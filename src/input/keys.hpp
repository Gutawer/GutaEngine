#pragma once
#include <unordered_map>
#include <string>

namespace input {
	enum class KeyCode {
		Unknown = -1,
		Space,
		Apostrophe,
		Comma,
		Minus,
		Dot, Period = Dot, FullStop = Dot,
		Slash,

		NumRow0, NumRow1, NumRow2, NumRow3, NumRow4,
		NumRow5, NumRow6, NumRow7, NumRow8, NumRow9,

		Semicolon,
		Equal,

		A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,

		LeftBracket,
		BackSlash,
		RightBracket,
		GraveAccent,
		Escape,
		Enter,
		Tab,
		Backspace,
		Insert,
		Delete,
		Right,
		Left,
		Up,
		Down,
		PageUp,
		PageDown,
		Home,
		End,
		CapsLock,
		ScrollLock,
		NumLock,
		PrintScreen,
		Pause,

		F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12, F13, F14, F15, F16, F17, F18, F19, F20, F21, F22, F23, F24, F25,

		NumPad0, NumPad1, NumPad2, NumPad3, NumPad4,
		NumPad5, NumPad6, NumPad7, NumPad8, NumPad9,

		NumPadDot, NumPadPeriod = NumPadDot, NumPadFullStop = NumPadDot,
		NumPadDivide,
		NumPadMultiply,
		NumPadMinus,
		NumPadPlus,
		NumPadEnter,
		NumPadEqual,

		LeftShift,
		LeftControl,
		LeftAlt,
		LeftSuper,

		RightShift,
		RightControl,
		RightAlt,
		RightSuper,

		Menu
	};

	//std::unordered_map<KeyCode, std::string> codeDisplayStringMap {
	//
	//};
}