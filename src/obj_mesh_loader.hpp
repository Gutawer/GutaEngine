#pragma once
#include "file.hpp"
#include "hash.hpp"
#include "graphics/render_mesh.hpp"
#include "mesh_loaders_common.hpp"
#include "maths/vector/vector.hpp"
#include <vector>
#include <unordered_map>

namespace meshloaders {
	namespace render {
		inline std::unique_ptr<RenderMesh> loadObj(filesystem::ResourceLocation location) {
			std::vector<std::array<uint32_t, 3>> vertices;
			std::vector<std::array<uint32_t, 3>> triangles;
			std::vector<Vector3F> vertPositions;
			std::vector<Vector3F> vertNormals;
			std::vector<Vector2F> vertUVs;

			std::ifstream stream(location.getAbsolutePath());
			std::string line;
			while (std::getline(stream, line)) {
				std::istringstream lineStream(line);
				std::string token;

				lineStream >> token;
				if (token[0] == '#') continue;
				else if (token == "v") {
					Vector3F vert;
					lineStream >> token;
					vert.x = std::stof(token);
					lineStream >> token;
					vert.y = std::stof(token);
					lineStream >> token;
					vert.z = std::stof(token);
					vertPositions.push_back(vert);
				}
				else if (token == "vn") {
					Vector3F normal;
					lineStream >> token;
					normal.x = std::stof(token);
					lineStream >> token;
					normal.y = std::stof(token);
					lineStream >> token;
					normal.z = std::stof(token);
					vertNormals.push_back(normal);
				}
				else if (token == "vt") {
					Vector2F uv;
					lineStream >> token;
					uv.x = std::stof(token);
					lineStream >> token;
					uv.y = std::stof(token);
					vertUVs.push_back(uv);
				}
				else if (token == "f") {
					std::array<uint32_t, 3> tri;
					for (int i = 0; i < 3; i++) {
						if (!(lineStream >> token)); // deal with later
						std::array<uint32_t, 3> vert;
						std::istringstream tokenStream(token);
						std::string elem;
						std::getline(tokenStream, elem, '/');
						vert[0] = std::stoi(elem);

						std::getline(tokenStream, elem, '/');
						vert[2] = std::stoi(elem);

						std::getline(tokenStream, elem, '/');
						vert[1] = std::stoi(elem);

						vertices.push_back(vert);
						tri[i] = vertices.size() - 1;
					}
					triangles.push_back(tri);
				}
			}

			auto ret = meshFromAttributes(vertices, triangles, vertPositions, vertNormals, vertUVs);
			addTangentVectorsToMesh(ret.get());
			return ret;
		}
	}

	namespace physics {
		inline std::shared_ptr<PhysicsMesh> loadObj(filesystem::ResourceLocation location) {
			std::vector<std::array<uint32_t, 3>> triangles;
			std::vector<Vector3F> vertPositions;

			std::ifstream stream(location.getAbsolutePath());
			std::string line;
			while (std::getline(stream, line)) {
				std::istringstream lineStream(line);
				std::string token;

				lineStream >> token;
				if (token[0] == '#') continue;
				else if (token == "v") {
					Vector3F vert;
					lineStream >> token;
					vert.x = std::stof(token);
					lineStream >> token;
					vert.y = std::stof(token);
					lineStream >> token;
					vert.z = std::stof(token);
					vertPositions.push_back(vert);
				}
				else if (token == "f") {
					std::array<uint32_t, 3> tri;
					for (int i = 0; i < 3; i++) {
						if (!(lineStream >> token)); // deal with later
						uint32_t vert;
						std::istringstream tokenStream(token);
						std::string elem;
						std::getline(tokenStream, elem, '/');
						vert = std::stoi(elem) - 1;

						std::getline(tokenStream, elem, '/');
						std::getline(tokenStream, elem, '/');

						tri[i] = vert;
					}
					triangles.push_back(tri);
				}
			}

			auto ret = std::make_shared<PhysicsMesh>(triangles, vertPositions);
			return ret;
		}
	}
}