#include "component.hpp"
#include "actor.hpp"
#include "game.hpp"

ActorComponent::~ActorComponent() {}

Actor* ActorComponent::getOwner() const {
	return owner;
}

void ActorComponent::destroy() {
	onDestroy();
	for (auto& comp : getOwner()->components) {
		if (comp.get() == this) {
			getOwner()->components.erase(comp);
			return;
		}
	}
}
