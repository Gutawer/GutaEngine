#pragma once

#include "maths/vector/vector.hpp"
#include "maths/matrix/matrix.hpp"
#include "maths/colour.hpp"
#include <GL/glew.h>
#include <vector>
#include <string>

namespace graphics {
	class OpenGLShaderManager;

	class OpenGLShader {
		friend class OpenGLShaderManager;
		friend class OpenGLShaderProgram;
	public:
		OpenGLShader(std::string srcString, GLenum type);
	private:
		///
		/// \brief Gets OpenGL's shader info log for a shader.
		///
		/// Gets OpenGL's shader info log for the shader passed in.
		///
		/// \param shader The shader to get the info log from
		///
		/// \return The shader's info log
		///
		std::string getShaderInfoLog(GLuint shader);

		GLuint shader;
	};

	class OpenGLShaderProgram {
		friend class OpenGLShaderManager;
	public:
		OpenGLShaderProgram(std::vector<OpenGLShader*> shaders);

		///
		/// \brief Returns if a shader program is valid.
		///
		/// Returns if a shader program is valid.
		///
		/// \param program The program to validate
		///
		/// \return True if the program is valid, else false
		///
		bool isValid();

		///
		/// \brief Logs the info for a shader program.
		///
		/// Logs the info for a shader program, optionally printing it to stdout as well.
		///
		/// \param program The shader program index to log from.
		/// \param print   Whether to also print to stdout
		///
		void logShaderProgramInfo(bool print);

		void setUniformFloat(const GLchar* name, float f) {
			GLint loc = glGetUniformLocation(program, name);
			glUniform1f(loc, f);
		}

		void setUniformVector3(const GLchar* name, Vector3F vec) {
			GLint loc = glGetUniformLocation(program, name);
			glUniform3f(loc, vec.x, vec.y, vec.z);
		}

		void setUniformColour3(const GLchar* name, Colour col) {
			GLint loc = glGetUniformLocation(program, name);
			glUniform3f(loc, col.r, col.g, col.b);
		}

		void setUniformColour4(const GLchar* name, Colour col) {
			GLint loc = glGetUniformLocation(program, name);
			glUniform4f(loc, col.r, col.g, col.b, col.a);
		}

		void setUniformMatrix3x3(const GLchar* name, Matrix3x3F mat) {
			GLint loc = glGetUniformLocation(program, name);
			float* matValues = mat.values.data();
			glUniformMatrix3fv(loc, 1, GL_FALSE, matValues);
		}

		void setUniformMatrix4x4(const GLchar* name, Matrix4x4F mat) {
			GLint loc = glGetUniformLocation(program, name);
			float* matValues = mat.values.data();
			glUniformMatrix4fv(loc, 1, GL_FALSE, matValues);
		}

		void use() {
			glUseProgram(program);
		}
	private:
		///
		/// \brief Gets OpenGL's shader program info log for a program.
		///
		/// Gets OpenGL's shader program info log for the program passed in.
		///
		/// \param program The program to get the info log from
		///
		/// \return The program's info log
		///
		std::string getShaderProgramInfoLog(GLuint program);

		std::vector<OpenGLShader*> shaders;
		GLuint program;
	};
}