#include "render_mesh.hpp"
#include "obj_mesh_loader.hpp"

std::unordered_map<std::filesystem::path, std::unique_ptr<RenderMesh>, filesystem::PathHash, filesystem::PathEquality> RenderMesh::meshes;

RenderMesh* RenderMesh::getMesh(filesystem::ResourceLocation path) {
	auto iter = meshes.find(path.getAbsolutePath());
	if (iter == meshes.end()) {
		std::unique_ptr<RenderMesh> mesh = meshloaders::render::loadObj(path);
		auto ptr = mesh.get();
		meshes[path.getAbsolutePath()] = std::move(mesh);
		return ptr;
	}
	else return iter->second.get();
}
