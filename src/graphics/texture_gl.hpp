#pragma once
#include "file.hpp"
#include "graphics/graphics.hpp"
#include "graphics/image.hpp"
#include <string>
#include <vector>

class OpenGLTexture {
	friend class OpenGLRenderer;
public:
	OpenGLTexture(GLenum unit, filesystem::ResourceLocation location, GLint internalFormat = GL_SRGB_ALPHA, GLint format = GL_RGBA) : image(location, true), unit(unit) {
		init(internalFormat, format);
	}
	OpenGLTexture(GLenum unit, Image image, GLint internalFormat = GL_SRGB_ALPHA, GLint format = GL_RGBA) : image(image), unit(unit) {
		init(internalFormat, format);
	}

	~OpenGLTexture() {
		glDeleteTextures(1, &id);
	}

	void bind() {
		glActiveTexture(unit);

		glBindTexture(GL_TEXTURE_2D, id);

		glActiveTexture(GL_TEXTURE0);
	}
private:
	GLenum unit;
	GLuint id;
	Image image;

	void init(GLint internalFormat, GLint format) {
		glActiveTexture(unit);

		glGenTextures(1, &id);
		glBindTexture(GL_TEXTURE_2D, id);
		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			internalFormat,
			image.width,
			image.height,
			0,
			format,
			GL_UNSIGNED_BYTE,
			image.imageData.data()
		);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		glGenerateMipmap(GL_TEXTURE_2D);

		GLfloat maxAniso = 0.0f;
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAniso);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAniso);

		glActiveTexture(GL_TEXTURE0);
	}
};