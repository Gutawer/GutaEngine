#include "graphics_gl.hpp"
#include "game.hpp"
#include "maths/maths.hpp"
#include "maths/vector/vector.hpp"
#include "graphics/surface.hpp"
#include "maths/matrix/matrix.hpp"
#include "maths/colour.hpp"
#include "graphics/light.hpp"
#include "graphics/texture_gl.hpp"
#include "graphics/render_mesh.hpp"
#include "obj_mesh_loader.hpp"
#include <functional>
#include <cmath>
#include <vector>

namespace graphics {
	// OpenGLShaderManager:

	OpenGLShader* OpenGLShaderManager::createShader(filesystem::ResourceLocation location, GLenum type) {
		auto iter = shaders.find(location.getAbsolutePath());
		if (iter == shaders.end()) {
			std::string srcString = ("#version 420\n");
			srcString += filesystem::readFileText(location.getAbsolutePath());
			auto ret = std::make_unique<OpenGLShader>(srcString, type);
			auto raw = ret.get();
			shaders[location.getAbsolutePath()] = std::move(ret);
			return raw;
		}
		else {
			return iter->second.get();
		}
	}

	OpenGLShaderProgram* OpenGLShaderManager::createShaderProgram(std::vector<OpenGLShader*> shaders) {
		auto ret = std::make_unique<OpenGLShaderProgram>(shaders);
		auto raw = ret.get();
		programs.push_back(std::move(ret));
		return raw;
	}

	OpenGLMeshData::OpenGLMeshData(RenderMesh* mesh) {
		glGenBuffers(1, &vertexBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, mesh->verts.size() * sizeof(RenderVertex), mesh->verts.data(), GL_STATIC_DRAW);

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(RenderVertex), reinterpret_cast<GLvoid*>(offsetof(RenderVertex, pos)));
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(RenderVertex), reinterpret_cast<GLvoid*>(offsetof(RenderVertex, normal)));
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(RenderVertex), reinterpret_cast<GLvoid*>(offsetof(RenderVertex, texCoords)));
		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(RenderVertex), reinterpret_cast<GLvoid*>(offsetof(RenderVertex, tangent)));

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);

		glGenBuffers(1, &indexBuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->surfaces.size() * sizeof(RenderSurface), mesh->surfaces.data(), GL_STATIC_DRAW);
	}

	void OpenGLMeshData::bind() {
		glBindVertexArray(vao);
	}

	// OpenGLRenderer:

	OpenGLRenderer::OpenGLRenderer(std::shared_ptr<window::WindowManager> window) {
		filesystem::restartLog(filesystem::LogType::Renderer);

		this->window = window;

		glewExperimental = GL_TRUE;
		glewInit();

		const GLubyte* renderer = glGetString(GL_RENDERER);
		const GLubyte* version = glGetString(GL_VERSION);

		filesystem::printAndLog(filesystem::LogType::Renderer, "Renderer: {}\n", gluByteToString(renderer));
		filesystem::printAndLog(filesystem::LogType::Renderer, "OpenGL supported version: {}\n", gluByteToString(version));

		logGlParams();

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);

		Vector2I fbSize = window->getFramebufferSize();
		deferredBuffer = std::make_unique<OpenGLFramebuffer>(fbSize, 6);
		litBuffer = std::make_unique<OpenGLFramebuffer>(fbSize, 1);

		sphereMesh = RenderMesh::getMesh(filesystem::ResourceLocation("models/sphere.obj"));

		tex0Diff     = std::make_unique<OpenGLTexture>(GL_TEXTURE0, filesystem::ResourceLocation("textures/test_diff.png"    ));
		tex0Spec     = std::make_unique<OpenGLTexture>(GL_TEXTURE1, filesystem::ResourceLocation("textures/test_spec.png"    ));
		tex0AO       = std::make_unique<OpenGLTexture>(GL_TEXTURE2, filesystem::ResourceLocation("textures/test_ao.png"      ));
		tex0Emission = std::make_unique<OpenGLTexture>(GL_TEXTURE3, filesystem::ResourceLocation("textures/test_emission.png"));
		tex0Normal   = std::make_unique<OpenGLTexture>(GL_TEXTURE4, filesystem::ResourceLocation("textures/test_normal.png"  ), GL_RGBA);

		static_assert(std::is_standard_layout<Vector2F>());
		static_assert(std::is_standard_layout<Vector3F>());

		static_assert(std::is_standard_layout<RenderVertex>());

		static_assert(std::is_standard_layout<RenderSurface>());

		static_assert(std::is_standard_layout<Matrix3x3F>());
		static_assert(std::is_standard_layout<Matrix4x4F>());

		try {
			standardVertexShader = shaderManager.createShader(filesystem::ResourceLocation("shaders/standard.vert"), GL_VERTEX_SHADER);
			standardFragmentShader = shaderManager.createShader(filesystem::ResourceLocation("shaders/standard.frag"), GL_FRAGMENT_SHADER);

			postProcessQuadVertexShader = shaderManager.createShader(filesystem::ResourceLocation("shaders/postprocessquad.vert"), GL_VERTEX_SHADER);

			ambientFragmentShader = shaderManager.createShader(filesystem::ResourceLocation("shaders/ambient.frag"), GL_FRAGMENT_SHADER);

			pointLightVertexShader = shaderManager.createShader(filesystem::ResourceLocation("shaders/pointlight.vert"), GL_VERTEX_SHADER);
			pointLightStencilFragmentShader = shaderManager.createShader(filesystem::ResourceLocation("shaders/pointlightstencil.frag"), GL_FRAGMENT_SHADER);
			pointLightFragmentShader = shaderManager.createShader(filesystem::ResourceLocation("shaders/pointlight.frag"), GL_FRAGMENT_SHADER);

			finalFragmentShader = shaderManager.createShader(filesystem::ResourceLocation("shaders/final.frag"), GL_FRAGMENT_SHADER);
		}
		catch (const filesystem::FileAccessException& e) {
			std::string msg = fmt::format("Failed to load shaders: \"{}\".\n", e.what());
			filesystem::errorLog(filesystem::LogType::Renderer, msg);
			throw RendererException(msg);
		}
		catch (const OpenGLShaderCompilationException& e) {
			std::string msg = e.what();
			filesystem::errorLog(filesystem::LogType::Renderer, msg);
			throw RendererException(msg);
		}

		try {
			standardShaderProgram = shaderManager.createShaderProgram({ standardVertexShader, standardFragmentShader });
			ambientShaderProgram = shaderManager.createShaderProgram({ postProcessQuadVertexShader, ambientFragmentShader });
			pointLightShaderProgram = shaderManager.createShaderProgram({ pointLightVertexShader, pointLightFragmentShader });
			pointLightStencilShaderProgram = shaderManager.createShaderProgram({ pointLightVertexShader, pointLightStencilFragmentShader });
			finalShaderProgram = shaderManager.createShaderProgram({ postProcessQuadVertexShader, finalFragmentShader });
		}
		catch (const OpenGLShaderProgramLinkException& e) {
			std::string msg = e.what();
			filesystem::errorLog(filesystem::LogType::Renderer, msg);
			throw RendererException(msg);
		}

		standardShaderProgram->logShaderProgramInfo(true);
		ambientShaderProgram->logShaderProgramInfo(true);
		pointLightShaderProgram->logShaderProgramInfo(true);
		pointLightStencilShaderProgram->logShaderProgramInfo(true);
		finalShaderProgram->logShaderProgramInfo(true);
	}

	void OpenGLRenderer::render(const Game& game, float fracTic) {
		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);
		glDisable(GL_BLEND);
		glDisable(GL_STENCIL_TEST);
		glCullFace(GL_BACK);
		glFrontFace(GL_CCW);
		glDepthMask(GL_TRUE);
		glDepthFunc(GL_LESS);

		if (tex0Diff     != nullptr) tex0Diff->bind();
		if (tex0Spec     != nullptr) tex0Spec->bind();
		if (tex0AO       != nullptr) tex0AO->bind();
		if (tex0Emission != nullptr) tex0Emission->bind();
		if (tex0Normal   != nullptr) tex0Normal->bind();

		Vector2I fbSize = window->getFramebufferSize();
		glViewport(0, 0, fbSize.x, fbSize.y);

		float gameTicWithFrac = game.getGameTic() + fracTic;

		standardShaderProgram->use();
		standardShaderProgram->setUniformFloat("timer", gameTicWithFrac);

		auto cam = game.getWorld()->player->getCameraComponent();
		if (cam != nullptr) {
			float aspect = static_cast<float>(fbSize.x) / fbSize.y;
			float fovy = 2 * std::atan(std::tan(std::clamp(cam->fov, 5.0f, 170.0f) / 2.0f * maths::constants::tau / 360.0f) / aspect);

			TransformF cameraTransform = cam->additionalTransformation * cam->getOwner()->transform;

			QuaternionF orientation = cameraTransform.orientation;
			Matrix4x4F view = Matrix4x4F::worldToView(orientation, cameraTransform.translation);
			Matrix4x4F proj = Matrix4x4F::perspective(fovy, aspect, cam->zNear, cam->zFar);

			standardShaderProgram->setUniformMatrix4x4("view", view);
			standardShaderProgram->setUniformMatrix4x4("proj", proj);

			standardShaderProgram->setUniformVector3("camPos", cameraTransform.translation);

			deferredBuffer->bind();

			glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

			for (auto& i : game.getWorld()->actors) {
				auto components = i->getComponents<RenderMeshComponent>();

				Matrix4x4F actorTransformMatrix = i->transform.toMatrix();
				standardShaderProgram->setUniformMatrix4x4("model", actorTransformMatrix);

				Matrix4x4F modelViewMatrix = view * actorTransformMatrix;
				standardShaderProgram->setUniformMatrix4x4("modelView", modelViewMatrix);

				Matrix3x3F normalMatrix = actorTransformMatrix.getLinearPart().inverse().transpose();
				standardShaderProgram->setUniformMatrix3x3("normal", normalMatrix);

				Vector3F scale = i->transform.scale;
				if (scale.x * scale.y * scale.z < 0.0f) glFrontFace(GL_CW);
				else                                    glFrontFace(GL_CCW);

				for (auto& c : components) {
					RenderMesh* mesh = c->getMesh();

					auto meshIter = meshRenderData.find(mesh);
					if (meshIter == meshRenderData.end()) {
						meshIter = meshRenderData.insert({ mesh, OpenGLMeshData(mesh) }).first;
					}
					meshIter->second.bind();

					glDrawElements(GL_TRIANGLES, mesh->surfaces.size() * 3, GL_UNSIGNED_INT, nullptr);
				}
			}

			if (game.getWorld()->lights.size() > 0) {
				auto meshIter = meshRenderData.find(sphereMesh);
				if (meshIter == meshRenderData.end()) {
					meshIter = meshRenderData.insert({ sphereMesh, OpenGLMeshData(sphereMesh) }).first;
				}
				meshIter->second.bind();
			}

			glDepthFunc(GL_LEQUAL);
			glDepthMask(GL_FALSE);

			glEnable(GL_STENCIL_TEST);
			glStencilFunc(GL_ALWAYS, 1, 0xFF);
			glStencilOp(GL_KEEP, GL_REPLACE, GL_KEEP);

			pointLightStencilShaderProgram->use();

			for (const auto& light : game.getWorld()->lights) {
				Vector3F lightPos = (light->transform * light->getOwner()->transform).translation;

				pointLightStencilShaderProgram->setUniformVector3("viewPos", cameraTransform.translation);

				pointLightStencilShaderProgram->setUniformVector3("lightPos", lightPos);
				pointLightStencilShaderProgram->setUniformColour3("lightColour", light->lightColour);
				pointLightStencilShaderProgram->setUniformFloat("lightIntensity", light->intensity);
				pointLightStencilShaderProgram->setUniformFloat("lightRange", light->range);

				pointLightStencilShaderProgram->setUniformMatrix4x4("view", view);
				pointLightStencilShaderProgram->setUniformMatrix4x4("proj", proj);

				glDrawElements(GL_TRIANGLES, sphereMesh->surfaces.size() * 3, GL_UNSIGNED_INT, nullptr);
			}

			deferredBuffer->copyDepthStencil(litBuffer.get());
			deferredBuffer->bindTexturesForDeferred();
			litBuffer->bind();
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

			glEnable(GL_BLEND);
			glBlendEquation(GL_ADD);
			glBlendFunc(GL_ONE, GL_ONE);

			glCullFace(GL_FRONT);
			glDepthFunc(GL_GEQUAL);

			glStencilFunc(GL_EQUAL, 0, 0xFF);
			glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

			pointLightShaderProgram->use();

			for (const auto& light : game.getWorld()->lights) {
				Vector3F lightPos = (light->transform * light->getOwner()->transform).translation;

				pointLightShaderProgram->setUniformVector3("viewPos", cameraTransform.translation);

				pointLightShaderProgram->setUniformVector3("lightPos", lightPos);
				pointLightShaderProgram->setUniformColour3("lightColour", light->lightColour);
				pointLightShaderProgram->setUniformFloat("lightIntensity", light->intensity);
				pointLightShaderProgram->setUniformFloat("lightRange", light->range);

				pointLightShaderProgram->setUniformMatrix4x4("view", view);
				pointLightShaderProgram->setUniformMatrix4x4("proj", proj);

				glDrawElements(GL_TRIANGLES, sphereMesh->surfaces.size() * 3, GL_UNSIGNED_INT, nullptr);
			}

			glDisable(GL_STENCIL_TEST);
			glDisable(GL_DEPTH_TEST);
			glCullFace(GL_BACK);

			ambientShaderProgram->use();

			ambientShaderProgram->setUniformColour3("ambientLightColour", Colour(0.05f, 0.05f, 0.05f));

			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

			litBuffer->bindTexturesForDeferred();
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			finalShaderProgram->use();
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}

		window->swapBuffers();
	}

	void OpenGLRenderer::logGlParams() {
		std::pair<GLenum, std::string> params[] = {
			{ GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, "GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS" },
			{ GL_MAX_CUBE_MAP_TEXTURE_SIZE,        "GL_MAX_CUBE_MAP_TEXTURE_SIZE"        },
			{ GL_MAX_DRAW_BUFFERS,                 "GL_MAX_DRAW_BUFFERS"                 },
			{ GL_MAX_FRAGMENT_UNIFORM_COMPONENTS,  "GL_MAX_FRAGMENT_UNIFORM_COMPONENTS"  },
			{ GL_MAX_TEXTURE_IMAGE_UNITS,          "GL_MAX_TEXTURE_IMAGE_UNITS"          },
			{ GL_MAX_TEXTURE_SIZE,                 "GL_MAX_TEXTURE_SIZE"                 },
			{ GL_MAX_VARYING_FLOATS,               "GL_MAX_VARYING_FLOATS"               },
			{ GL_MAX_VERTEX_ATTRIBS,               "GL_MAX_VERTEX_ATTRIBS"               },
			{ GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS,   "GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS"   },
			{ GL_MAX_VERTEX_UNIFORM_COMPONENTS,    "GL_MAX_VERTEX_UNIFORM_COMPONENTS"    },
			{ GL_MAX_VIEWPORT_DIMS,                "GL_MAX_VIEWPORT_DIMS"                },
			{ GL_STEREO,                           "GL_STEREO"                           }
		};

		filesystem::log(filesystem::LogType::Renderer, "\nGL context params:\n");
		for (int i = 0; i < 10; i++) {
			int v = 0;
			glGetIntegerv(std::get<0>(params[i]), &v);
			filesystem::log(filesystem::LogType::Renderer, fmt::format("{}: {}\n", std::get<1>(params[i]), v));
		}
		int v[2] = { 0 };
		glGetIntegerv(std::get<0>(params[10]), v);
		filesystem::log(filesystem::LogType::Renderer, fmt::format("{}: {}, {}\n", std::get<1>(params[10]), v[0], v[1]));

		GLboolean s = false;
		glGetBooleanv(std::get<0>(params[11]), &s);
		filesystem::log(filesystem::LogType::Renderer, fmt::format("{}: {}\n-----------------------------\n", std::get<1>(params[11]), static_cast<bool>(s)));
	}

	std::string gluByteToString(const GLubyte* text) {
		return static_cast<std::string>(reinterpret_cast<const char*>(text));
	}

	std::string glTypeToString(GLenum type) {
		switch (type) {
		case GL_BOOL:              return "bool";
		case GL_INT:               return "int";
		case GL_FLOAT:             return "float";
		case GL_FLOAT_VEC2:        return "vec2";
		case GL_FLOAT_VEC3:        return "vec3";
		case GL_FLOAT_VEC4:        return "vec4";
		case GL_FLOAT_MAT2:        return "mat2";
		case GL_FLOAT_MAT3:        return "mat3";
		case GL_FLOAT_MAT4:        return "mat4";
		case GL_SAMPLER_2D:        return "sampler2D";
		case GL_SAMPLER_3D:        return "sampler3D";
		case GL_SAMPLER_CUBE:      return "samplerCube";
		case GL_SAMPLER_2D_SHADOW: return "sampler2DShadow";
		default: break;
		}
		return "other";
	}
}
