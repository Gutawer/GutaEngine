#include "light.hpp"
#include "actor.hpp"

void RenderLightComponent::beginPlay() {
	world = getOwner()->getWorldWeak();
	if (auto sharedWorld = world.lock()) {
		sharedWorld->lights.insert(this);
	}
}

void RenderLightComponent::onDestroy() {
	if (auto sharedWorld = world.lock()) {
		sharedWorld->lights.erase(this);
	}
}

void DynamicLight::beginPlay() {
	lightComponent = addComponent<RenderLightComponent>();
}

RenderLightComponent* DynamicLight::getLightComponent() {
	return lightComponent;
}
