#pragma once
#include "graphics/graphics.hpp"
#include "graphics/render_mesh.hpp"
#include "graphics/texture_gl.hpp"
#include "graphics/framebuffer_gl.hpp"
#include "graphics/shader_gl.hpp"
#include "window/window.hpp"
#include "file.hpp"
#include <filesystem>
#include <string>
#include <memory>
#include <vector>
#include <unordered_map>

namespace filesystem {
	class ResourceLocation;
}

namespace graphics {
	/// Exception for OpenGL shader compilation failures.
	class OpenGLShaderCompilationException : public RendererException {
	public:
		///
		/// \brief Creates an OpenGLShaderCompilationException.
		///
		/// Creates an OpenGLShaderCompilationException with a given message.
		///
		/// \param message The message to use
		///
		OpenGLShaderCompilationException(std::string msg) : RendererException(msg) {}
	};

	/// Exception for OpenGL shader program linking failures.
	class OpenGLShaderProgramLinkException : public RendererException {
	public:
		///
		/// \brief Creates an OpenGLShaderProgramLinkException.
		///
		/// Creates an OpenGLShaderProgramLinkException with a given message.
		///
		/// \param message The message to use
		///
		OpenGLShaderProgramLinkException(std::string msg) : RendererException(msg) {}
	};

	/// A helper class for managing shaders.
	class OpenGLShaderManager {
	public:
		///
		/// \brief Creates a shader from a filename and type.
		///
		/// Creates a shader from a filename and type, re-using shaders that have already been created when possible.
		///
		/// \param filename The file to read from for the shader
		/// \param type     The type of shader to create (GL_VERTEX_SHADER, GL_FRAGMENT_SHADER, etc.)
		///
		/// \return The shader's integer to be referenced by
		///
		OpenGLShader* createShader(filesystem::ResourceLocation location, GLenum type);

		///
		/// \brief Creates a shader program from a set of shaders.
		///
		/// Creates a shader program from a set of shaders.
		///
		/// \param shaders A vector of shader indices to use with program creation
		///
		/// \return The program's integer to be referenced by
		///
		OpenGLShaderProgram* createShaderProgram(std::vector<OpenGLShader*> shaders);
	private:
		std::unordered_map<std::filesystem::path, std::unique_ptr<OpenGLShader>, filesystem::PathHash, filesystem::PathEquality> shaders;
		std::vector<std::unique_ptr<OpenGLShaderProgram>> programs;
	};

	class OpenGLMeshData {
	public:
		GLuint vao;
		GLuint vertexBuffer;
		GLuint indexBuffer;

		OpenGLMeshData(RenderMesh* mesh);

		void bind();
	};

	/// OpenGLRenderer class for rendering via OpenGL.
	class OpenGLRenderer : public Renderer {
		friend class OpenGLShaderManager;
	public:
		///
		/// \brief Creates an OpenGLRenderer.
		///
		/// Creates an OpenGLRenderer with a window.
		///
		/// \param window The window being drawn to (necessary for updating the render canvas size).
		///
		OpenGLRenderer(std::shared_ptr<window::WindowManager> window);

		void render(const Game& game, float fracTic) override;
	private:
		///
		/// \brief Logs a bunch of OpenGL parameters.
		///
		/// Logs a bunch of OpenGL parameters (e.g. GL_MAX_TEXTURE_SIZE) for helping with debugging.
		///
		void logGlParams();

		OpenGLShader* standardVertexShader;
		OpenGLShader* standardFragmentShader;
		OpenGLShaderProgram* standardShaderProgram;

		OpenGLShader* postProcessQuadVertexShader;

		OpenGLShader* ambientFragmentShader;
		OpenGLShaderProgram* ambientShaderProgram;

		OpenGLShader* pointLightVertexShader;
		OpenGLShader* pointLightStencilFragmentShader;
		OpenGLShader* pointLightFragmentShader;
		OpenGLShaderProgram* pointLightStencilShaderProgram;
		OpenGLShaderProgram* pointLightShaderProgram;

		OpenGLShader* finalFragmentShader;
		OpenGLShaderProgram* finalShaderProgram;

		std::unique_ptr<OpenGLFramebuffer> deferredBuffer;
		std::unique_ptr<OpenGLFramebuffer> litBuffer;

		std::unique_ptr<OpenGLTexture> tex0Diff;
		std::unique_ptr<OpenGLTexture> tex0Spec;
		std::unique_ptr<OpenGLTexture> tex0AO;
		std::unique_ptr<OpenGLTexture> tex0Emission;
		std::unique_ptr<OpenGLTexture> tex0Normal;

		OpenGLShaderManager shaderManager;

		std::unordered_map<RenderMesh*, OpenGLMeshData> meshRenderData;
		RenderMesh* sphereMesh;
	};

	///
	/// \brief Converts a pointer to a GLubyte* to a C++ string.
	///
	/// Converts a pointer to a GLubyte* to a C++ string.
	///
	/// \param text The pointer to convert from
	///
	/// \return The created string
	///
	std::string gluByteToString(const GLubyte* text);

	///
	/// \brief Gives a string representation of common GLSL types.
	///
	/// Gives a string representation of common GLSL types (e.g. GL_FLOAT_VEC2 -> vec2).
	///
	/// \param type The GLenum type to convert from
	///
	/// \return The string representation of type
	///
	std::string glTypeToString(GLenum type);
}