#include "shader_gl.hpp"
#include "graphics_gl.hpp"

graphics::OpenGLShader::OpenGLShader(std::string srcString, GLenum type) {
	shader = glCreateShader(type);
	auto src = srcString.c_str();
	glShaderSource(shader, 1, &src, nullptr);
	glCompileShader(shader);

	GLint compileOk = -1;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compileOk);
	if (compileOk != GL_TRUE) {
		std::string msg = fmt::format("Compilation of shader (index {}) failed:\n{}\n", shader, getShaderInfoLog(shader));
		throw OpenGLShaderCompilationException(msg);
	}
}

std::string graphics::OpenGLShader::getShaderInfoLog(GLuint shader) {
	const int maxLength = 2048;
	int actualLength = 0;
	char log[2048];
	glGetShaderInfoLog(shader, maxLength, &actualLength, log);
	std::string msg = fmt::format("Info log for shader index {}: {}.", shader, log);
	return msg;
}

graphics::OpenGLShaderProgram::OpenGLShaderProgram(std::vector<OpenGLShader*> shaders) : shaders(shaders) {
	program = glCreateProgram();
	for (auto s : shaders) {
		glAttachShader(program, s->shader);
	}
	glLinkProgram(program);

	GLint linkOk = -1;
	glGetProgramiv(program, GL_LINK_STATUS, &linkOk);
	if (linkOk != GL_TRUE) {
		std::string msg = fmt::format("Linking of program shader (index {}) failed:\n{}\n", program, getShaderProgramInfoLog(program));
		throw OpenGLShaderProgramLinkException(msg);
	}
}

std::string graphics::OpenGLShaderProgram::getShaderProgramInfoLog(GLuint program) {
	const int maxLength = 2048;
	int actualLength = 0;
	char log[2048] = { '\0' };
	glGetProgramInfoLog(program, maxLength, &actualLength, log);
	std::string msg = fmt::format("Info log for shader program index {}: {}.", program, log);
	return msg;
}

bool graphics::OpenGLShaderProgram::isValid() {
	glValidateProgram(program);
	GLint params = -1;
	glGetProgramiv(program, GL_VALIDATE_STATUS, &params);
	return static_cast<bool>(params);
}

void graphics::OpenGLShaderProgram::logShaderProgramInfo(bool print) {
	std::function<void(std::string)> logFunc;
	using namespace std::placeholders;
	if (!print) logFunc = std::bind(filesystem::log<>, filesystem::LogType::Renderer, _1);
	else        logFunc = std::bind(filesystem::printAndLog<>, filesystem::LogType::Renderer, _1);

	logFunc(fmt::format("--------------------\nShader program {} info:\n", program));

	GLint params = -1;
	glGetProgramiv(program, GL_LINK_STATUS, &params);
	logFunc("GL_LINK_STATUS: " + static_cast<std::string>((params == GL_TRUE ? "true\n" : "false\n")));

	glGetProgramiv(program, GL_ATTACHED_SHADERS, &params);
	logFunc("GL_ATTACHED_SHADERS: " + std::to_string(params) + "\n");

	glGetProgramiv(program, GL_ACTIVE_ATTRIBUTES, &params);
	logFunc("GL_ACTIVE_ATTRIBUTES: " + std::to_string(params) + "\n");
	for (GLint i = 0; i < params; i++) {
		const int maxLength = 64;
		int actualLength = 0;
		int size = 0;
		GLenum type;
		char name[64] = { '\0' };

		glGetActiveAttrib(program, i, maxLength, &actualLength, &size, &type, name);
		// attribute with multiple values
		if (size > 1) {
			for (int j = 0; j < size; j++) {
				std::string longName = static_cast<std::string>(name) + "[" + std::to_string(j) + "]";
				const char* longNameCStr = longName.c_str();
				GLint location = glGetAttribLocation(program, longNameCStr);
				logFunc(fmt::format("Attrib {}: type: {}, name: {}, location: {}\n", i, glTypeToString(type), longName, location));
			}
		}
		else {
			GLint location = glGetAttribLocation(program, name);
			logFunc(fmt::format("Attrib {}: type: {}, name: {}, location: {}\n", i, glTypeToString(type), name, location));
		}
	}

	glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &params);
	logFunc("GL_ACTIVE_UNIFORMS: " + std::to_string(params) + "\n");
	for (GLint i = 0; i < params; i++) {
		const int maxLength = 64;
		int actualLength = 0;
		int size = 0;
		GLenum type;
		char name[64] = { '\0' };

		glGetActiveUniform(program, i, maxLength, &actualLength, &size, &type, name);
		// uniform with multiple values
		if (size > 1) {
			for (int j = 0; j < size; j++) {
				std::string longName = static_cast<std::string>(name) + "[" + std::to_string(j) + "]";
				const char* longNameCStr = longName.c_str();
				GLint location = glGetUniformLocation(program, longNameCStr);
				logFunc(fmt::format("Uniform {}: type: {}, name: {}, location: {}\n", i, glTypeToString(type), longName, location));
			}
		}
		else {
			GLint location = glGetUniformLocation(program, name);
			logFunc(fmt::format("Uniform {}: type: {}, name: {}, location: {}\n", i, glTypeToString(type), name, location));
		}
	}

	logFunc(getShaderProgramInfoLog(program) + "\n");
}
