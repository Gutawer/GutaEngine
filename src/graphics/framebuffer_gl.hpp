#pragma once

#include "file.hpp"
#include "graphics/graphics.hpp"
#include <string>
#include <vector>

class OpenGLFramebuffer {
	friend class OpenGLRenderer;
public:
	OpenGLFramebuffer(Vector2I dimensions, size_t num, GLint internalFormat = GL_RGBA16F, GLint format = GL_RGBA, bool depthStencil = true) : num(num), dimensions(dimensions) {
		glGenFramebuffers(1, &fbId);
		glBindFramebuffer(GL_FRAMEBUFFER, fbId);

		for (size_t i = 0; i < num; i++) {
			GLuint id = 0;
			glGenTextures(1, &id);
			texIds.push_back(id);
			glBindTexture(GL_TEXTURE_2D, id);
			glTexImage2D(
				GL_TEXTURE_2D,
				0,
				internalFormat,
				dimensions.x,
				dimensions.y,
				0,
				format,
				GL_UNSIGNED_BYTE,
				nullptr
			);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

			glFramebufferTexture2D(GL_FRAMEBUFFER, colourAttachments[i], GL_TEXTURE_2D, id, 0);
		}

		if (depthStencil) {
			GLuint id = 0;
			glGenTextures(1, &id);
			texIds.push_back(id);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, id);
			glTexImage2D(
				GL_TEXTURE_2D,
				0,
				GL_DEPTH24_STENCIL8,
				dimensions.x,
				dimensions.y,
				0,
				GL_DEPTH_COMPONENT,
				GL_UNSIGNED_BYTE,
				nullptr
			);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, id, 0);
		}
	}

	void bind() {
		glBindFramebuffer(GL_FRAMEBUFFER, fbId);
		assert(num > 0 && num <= 16);
		glDrawBuffers(num, colourAttachments.data());
	}

	void copyDepthStencil(OpenGLFramebuffer* to) {
		glBlitNamedFramebuffer(fbId, to->fbId, 0, 0, dimensions.x, dimensions.y, 0, 0, dimensions.x, dimensions.y, GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT, GL_NEAREST);
	}

	void bindTexturesForDeferred() {
		for (size_t i = 0; i < num; i++) {
			glActiveTexture(textures[i]);
			glBindTexture(GL_TEXTURE_2D, texIds[i]);
		}
	}

	~OpenGLFramebuffer() {
		for (auto i : texIds) {
			glDeleteTextures(1, &i);
		}
		glDeleteFramebuffers(1, &fbId);
	}
private:
	static constexpr std::array<GLuint, 16> colourAttachments {
		GL_COLOR_ATTACHMENT0 , GL_COLOR_ATTACHMENT1 , GL_COLOR_ATTACHMENT2 , GL_COLOR_ATTACHMENT3 ,
		GL_COLOR_ATTACHMENT4 , GL_COLOR_ATTACHMENT5 , GL_COLOR_ATTACHMENT6 , GL_COLOR_ATTACHMENT7 ,
		GL_COLOR_ATTACHMENT8 , GL_COLOR_ATTACHMENT9 , GL_COLOR_ATTACHMENT10, GL_COLOR_ATTACHMENT11,
		GL_COLOR_ATTACHMENT12, GL_COLOR_ATTACHMENT13, GL_COLOR_ATTACHMENT14, GL_COLOR_ATTACHMENT15
	};

	static constexpr std::array<GLuint, 16> textures{
		GL_TEXTURE0 , GL_TEXTURE1 , GL_TEXTURE2 , GL_TEXTURE3 ,
		GL_TEXTURE4 , GL_TEXTURE5 , GL_TEXTURE6 , GL_TEXTURE7 ,
		GL_TEXTURE8 , GL_TEXTURE9 , GL_TEXTURE10, GL_TEXTURE11,
		GL_TEXTURE12, GL_TEXTURE13, GL_TEXTURE14, GL_TEXTURE15
	};

	Vector2I dimensions;
	std::vector<GLuint> texIds;
	GLuint fbId;
	size_t num;
};