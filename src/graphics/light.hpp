#pragma once
#include "component.hpp"
#include "maths/transform.hpp"
#include "maths/colour.hpp"
#include "actor.hpp"
#include <memory>

class World;

class RenderLightComponent : public ActorComponent {
public:
	RenderLightComponent(Actor* owner) : ActorComponent(owner) {}
	void beginPlay() override;

	void onDestroy() override;

	TransformF transform;
	Colour lightColour;
	float intensity = 0.0f;
	float range = 0.0f;
private:
	std::weak_ptr<World> world;
};

class DynamicLight : public Actor {
public:
	DynamicLight(std::shared_ptr<World> world) : Actor(world) {}

	void beginPlay() override;

	RenderLightComponent* getLightComponent();
private:
	RenderLightComponent* lightComponent = nullptr;
};