#pragma once
#include "maths/vector/vector3.hpp"
#include "maths/colour.hpp"

class RenderVertex {
public:
	Vector3F pos;
	Vector3F normal;
	Vector2F texCoords;
	Vector4F tangent;

	RenderVertex(Vector3F pos, Vector3F normal, Vector2F texCoords) : pos(pos), normal(normal), texCoords(texCoords) {}
	RenderVertex() {}
};