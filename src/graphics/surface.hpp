#pragma once
#include "graphics/vertex.hpp"
#include <array>

class RenderSurface {
public:
	std::array<uint32_t, 3> indices;

	RenderSurface(uint32_t index0, uint32_t index1, uint32_t index2) : indices{ index0, index1, index2 } {}
	RenderSurface() : indices{ 0 } {}
};