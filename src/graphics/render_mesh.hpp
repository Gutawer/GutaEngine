#pragma once
#include "maths/vector/vector.hpp"
#include "graphics/vertex.hpp"
#include "graphics/surface.hpp"
#include "component.hpp"
#include "file.hpp"
#include <memory>
#include <filesystem>
#include <unordered_map>
#include <vector>

class RenderMesh {
public:
	std::vector<RenderSurface> surfaces;
	std::vector<RenderVertex> verts;

	static RenderMesh* getMesh(filesystem::ResourceLocation path);
private:
	static std::unordered_map<std::filesystem::path, std::unique_ptr<RenderMesh>, filesystem::PathHash, filesystem::PathEquality> meshes;
};

class RenderMeshComponent : public ActorComponent {
public:
	RenderMeshComponent(Actor* owner, RenderMesh* mesh) : ActorComponent(owner), mesh(mesh) {}

	RenderMesh* getMesh() {
		return mesh;
	}
private:
	RenderMesh* mesh;
};