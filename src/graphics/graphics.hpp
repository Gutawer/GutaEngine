#pragma once
#include "window/window.hpp"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdexcept>
#include <memory>

class Game;

namespace graphics {
	/// Generic exception for renderer failures.
	class RendererException : public std::runtime_error {
	public:
		///
		/// \brief Creates a RendererException.
		/// 
		/// Creates a RendererException with a given message.
		///
		/// \param message The message to use
		///
		RendererException(std::string message) : runtime_error(message) {}
	};

	/// Renderer abstract class for a modular renderer system.
	class Renderer {
	protected:
		std::shared_ptr<window::WindowManager> window;
	public:
		///
		/// \brief Renders a game's scene.
		///
		/// Renders a game's scene.
		///
		virtual void render(const Game& game, float fracTic) = 0;
	};
}