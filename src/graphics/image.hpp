#pragma once
#include "file.hpp"
#include <vector>
#include <string>
#include <filesystem>
#include <fmt/format.h>
#include <stb_image.h>

class ImageLoadingException : public std::runtime_error {
public:
	ImageLoadingException(std::string fileName) : std::runtime_error("Failed to load image file at \"" + fileName + "\".") {}
};

class OpenGLTexture;

class Image {
	friend class OpenGLTexture;
public:
	Image(filesystem::ResourceLocation location, bool flipVertically) {
		std::string fileName = location.getAbsolutePath().string();
		int width, height, channels;
		uint8_t* data = stbi_load(fileName.c_str(), &width, &height, &channels, 4);
		if (data == nullptr) throw ImageLoadingException(fileName);

		imageData.assign(data, data + width * height * 4);
		this->width = width;
		this->height = height;

		stbi_image_free(data);

		if (flipVertically) {
			int byteWidth = width * 4;
			for (int row = 0; row < height / 2; row++) {
				int top = row * byteWidth;
				int bottom = (height - row - 1) * byteWidth;
				if (top != bottom) std::swap_ranges(imageData.begin() + top, imageData.begin() + top + byteWidth, imageData.begin() + bottom);
			}
		}
	}
private:
	int width = 0;
	int height = 0;
	std::vector<uint8_t> imageData;
};