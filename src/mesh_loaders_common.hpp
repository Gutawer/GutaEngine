#pragma once
#include "file.hpp"
#include "hash.hpp"
#include "graphics/render_mesh.hpp"
#include "maths/physics_mesh.hpp"
#include "maths/vector/vector.hpp"
#include <vector>
#include <unordered_map>

namespace meshloaders {
	namespace render {
		inline std::unique_ptr<RenderMesh> meshFromAttributes(const std::vector<std::array<uint32_t, 3>>& vertices, const std::vector<std::array<uint32_t, 3>>& triangles,
			                                                  const std::vector<Vector3F>& vertPositions, const std::vector<Vector3F>& vertNormals, const std::vector<Vector2F>& vertUVs) {
			std::vector<RenderSurface> finalSurfaces;
			std::vector<RenderVertex> finalVertices;
			auto hashFunc = [](const std::array<uint32_t, 3>& h) {
				std::size_t ret = 0;
				hashCombine(ret, h[0]);
				hashCombine(ret, h[1]);
				hashCombine(ret, h[2]);
				return ret;
			};
			std::unordered_map<std::array<uint32_t, 3>, uint32_t, decltype(hashFunc)> vertexIndices(0, hashFunc);
			for (const auto& t : triangles) {
				RenderSurface surface;
				for (int i = 0; i < t.size(); i++) {
					auto vert = vertices[t[i]];
					auto finalVert = vertexIndices.find(vert);
					if (finalVert == vertexIndices.end()) {
						finalVertices.push_back(
							RenderVertex(vertPositions[vert[0] - 1], vertNormals[vert[1] - 1].unit(), vertUVs[vert[2] - 1])
						);
						vertexIndices[vert] = finalVertices.size() - 1;
						surface.indices[i] = finalVertices.size() - 1;
					}
					else {
						surface.indices[i] = finalVert->second;
					}
				}
				finalSurfaces.push_back(surface);
			}

			std::unique_ptr<RenderMesh> mesh = std::make_unique<RenderMesh>();
			mesh->surfaces = finalSurfaces;
			mesh->verts = finalVertices;
			return mesh;
		}

		inline void addTangentVectorsToMesh(RenderMesh* mesh) {
			std::vector<Vector3F> tangents(mesh->verts.size());
			std::vector<Vector3F> bitangents(mesh->verts.size());

			for (const auto& t : mesh->surfaces) {
				uint32_t i0 = t.indices[0];
				uint32_t i1 = t.indices[1];
				uint32_t i2 = t.indices[2];

				const Vector3F& p0 = mesh->verts[i0].pos;
				const Vector3F& p1 = mesh->verts[i1].pos;
				const Vector3F& p2 = mesh->verts[i2].pos;

				const Vector2F& uv0 = mesh->verts[i0].texCoords;
				const Vector2F& uv1 = mesh->verts[i1].texCoords;
				const Vector2F& uv2 = mesh->verts[i2].texCoords;

				const Vector3F v0 = p1 - p0;
				const Vector3F v1 = p2 - p0;

				const Vector2F uvv0 = uv1 - uv0;
				const Vector2F uvv1 = uv2 - uv0;

				float r = 1.0f / (uvv0.x * uvv1.y - uvv1.x * uvv0.y);
				const Vector3F sdir = (uvv1.y * v0 - uvv0.y * v1) * r;
				const Vector3F tdir = (uvv0.x * v1 - uvv1.x * v0) * r;

				tangents[i0] += sdir;
				tangents[i1] += sdir;
				tangents[i2] += sdir;

				bitangents[i0] += tdir;
				bitangents[i1] += tdir;
				bitangents[i1] += tdir;
			}

			std::vector<Vector4F> finalTangents(mesh->surfaces.size() * 3);

			for (int i = 0; i < mesh->verts.size(); i++) {
				const Vector3F& n = mesh->verts[i].normal;
				const Vector3F& t = tangents[i];

				finalTangents[i].xyz = (t - n * n.dot(t)).unit();
				finalTangents[i].w = (n.cross(t).dot(bitangents[i]) < 0.0f) ? -1.0f : 1.0f;

				mesh->verts[i].tangent = finalTangents[i];
			}
		}
	}
}