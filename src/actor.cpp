#include "actor.hpp"
#include "game.hpp"

std::shared_ptr<World> Actor::getWorldShared() {
	if (auto ret = world.lock()) {
		return ret;
	}
	else throw GameException("Fatal error: Actor's world was expired on access.\n");
}

std::weak_ptr<World> Actor::getWorldWeak() {
	return world;
}

Actor::~Actor() {
	for (auto& i : components) {
		i->onDestroy();
	}
}

void Actor::destroy() {
	onDestroy();
	if (auto sharedWorld = world.lock()) {
		auto iter = std::remove_if(sharedWorld->actors.begin(), sharedWorld->actors.end(),
			[&](const std::unique_ptr<Actor>& x) { return x.get() == this; }
		);
	}
}
