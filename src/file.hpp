/// @file

#pragma once
#include "date.h"
#include <filesystem>
#include <fstream>
#include "fmt/format.h"
#include <sstream>
#include <string>
#include <unordered_map>
#include <chrono>
#include <stdexcept>

namespace filesystem {
	/// An exception thrown when file access fails.
	class FileAccessException : public std::runtime_error {
	public:
		FileAccessException(std::string fileName) : std::runtime_error("Failed to open file \"" + fileName + "\".") {}
	};

	class ResourceLocation {
	public:
		ResourceLocation(std::string relativePath) {
			namespace fs = std::filesystem;
			absolutePath = fs::path("ge_common") / relativePath;
		}

		std::filesystem::path getAbsolutePath() {
			return absolutePath;
		}
	private:
		std::filesystem::path absolutePath;
	};

	/// An enum for files for systems to log to.
	enum class LogType {
		Misc,
		Window,
		Renderer
	};

	static std::unordered_map<LogType, std::string> logPaths = {
		{ LogType::Misc    , "log_misc.txt"     },
		{ LogType::Window  , "log_window.txt"   },
		{ LogType::Renderer, "log_renderer.txt" }
	};

	///
	/// \brief Restarts the log given.
	/// 
	/// Restarts the log given, deleting all of the text inside and writing the current date and time.
	///
	/// \param which Which log to restart
	///
	inline void restartLog(LogType which) {
		std::string path = logPaths[which];
		std::ofstream file(path);
		if (file.fail()) {
			throw FileAccessException(path);
		}
		// required to make date.h's stream operator for time_point work
		using namespace date;
		file << "Log file created at " << std::chrono::system_clock::now() << "\n";
	}

	///
	/// \brief Logs to the log given.
	/// 
	/// Logs to the log given, appending the text at the end.
	///
	/// \param which Which log to log to
	/// \param text  The text to append
	///
	template <typename... Args>
	inline void log(LogType which, std::string text, const Args& ... args) {
		std::string path = logPaths[which];
		std::ofstream file(path, std::ios_base::app);
		if (file.fail()) {
			throw FileAccessException(path);
		}
		std::string formatted = fmt::format(text, args...);
		file << formatted;
	}

	///
	/// \brief Logs to the log given, and prints the text.
	/// 
	/// Logs to the log given, appending the text at the end, and prints the text to stdout.
	///
	/// \param which Which log to log to
	/// \param text  The text to append
	///
	template <typename... Args>
	inline void printAndLog(LogType which, std::string text, const Args& ... args) {
		std::string formatted = fmt::format(text, args...);
		log(which, formatted);
		fmt::print(formatted);
	}

	///
	/// \brief Logs to the log given, and gives an error message.
	/// 
	/// Logs to the log given, appending the text at the end, and gives an error message in stderr.
	///
	/// \param which Which log to log to
	/// \param text  The text to append
	///
	template <typename... Args>
	inline void errorLog(LogType which, std::string text, const Args& ... args) {
		std::string formatted = fmt::format(text, args...);
		log(which, formatted);
		fmt::print(stderr, formatted);
	}

	///
	/// \brief Reads the text of a file.
	///
	/// Reads the text of a file.
	/// 
	/// \param path The path to read from
	///
	/// \return The file text
	/// 
	/// \throws filesystem::FileAccessException
	///
	inline std::string readFileText(std::filesystem::path path) {
		std::ifstream inFile(path);
		if (inFile.fail()) {
			throw FileAccessException(path.string());
		}
		std::string fileText((std::istreambuf_iterator<char>(inFile)), std::istreambuf_iterator<char>());
		return fileText;
	}

	struct PathHash {
		std::size_t operator()(const std::filesystem::path& k) const {
			return std::filesystem::hash_value(k);
		}
	};

	struct PathEquality {
		bool operator()(const std::filesystem::path& l, const std::filesystem::path& r) const {
			return std::filesystem::equivalent(l, r);
		}
	};
};