#include "player.hpp"
#include "maths/transform.hpp"
#include "maths/matrix/matrix.hpp"
#include "graphics/light.hpp"
#include <iostream>

void PlayerPawn::beginPlay() {
	inputComponent = addComponent<input::InputComponent>();
	setupInputComponent();

	cameraComponent = addComponent<CameraComponent>();
	cameraComponent->additionalTransformation.translation = Vector3F(0.0f, 0.0f, 48.0f);
	cameraComponent->fov = 90.0f;

	transform.translation = Vector3F(-256.0f, 0.0f, 0.0f);
}

void PlayerPawn::setupInputComponent() {
	inputComponent->bindAxis("moveForward",  [this](float axisAmount) { moveForward(axisAmount);  });
	inputComponent->bindAxis("moveSideways", [this](float axisAmount) { moveSideways(axisAmount); });
	inputComponent->bindAxis("moveUpwards",  [this](float axisAmount) { moveUpwards(axisAmount);  });

	inputComponent->bindAxis("changeYaw",    [this](float axisAmount) { changeYaw(axisAmount);    });
	inputComponent->bindAxis("changePitch",  [this](float axisAmount) { changePitch(axisAmount);  });
	inputComponent->bindAxis("changeRoll",   [this](float axisAmount) { changeRoll(axisAmount);   });

	inputComponent->bindAction("resetPosOrientation", input::ActionType::Pressed, [this] { resetPosOrientation(); });
}

void PlayerPawn::moveForward(float axisAmount) {
	if (!resettingPosOrientation) moveVec.x = axisAmount;
}

void PlayerPawn::moveSideways(float axisAmount) {
	if (!resettingPosOrientation) moveVec.y = -axisAmount;
}

void PlayerPawn::moveUpwards(float axisAmount) {
	if (!resettingPosOrientation) transform.translation.z += axisAmount;
}

void PlayerPawn::changeYaw(float axisAmount) {
	if (!resettingPosOrientation) yaw += axisAmount;
}

void PlayerPawn::changePitch(float axisAmount) {
	if (!resettingPosOrientation) pitch += axisAmount;
}

void PlayerPawn::changeRoll(float axisAmount) {
	if (!resettingPosOrientation) roll += axisAmount;
}

void PlayerPawn::resetPosOrientation() {
	if (!resettingPosOrientation) {
		resettingPosOrientation = true;
		resetPos = transform.translation;
		resetOrientation = transform.orientation;
	}
}

void PlayerPawn::tick() {
	if (!resettingPosOrientation) {
		transform.orientation = QuaternionF::yawPitchRoll(yaw, pitch, roll);
		Vector3F rotatedMoveVec = transform.orientation * moveVec;
		if (moveVec.lengthSquared() > 1.0f) rotatedMoveVec = rotatedMoveVec.unit();
		transform.translation += rotatedMoveVec;
	}
	else {
		resettingPosOrientationProgress += 1.0f / 60;

		float progressSmoothed = maths::smoothstep(resettingPosOrientationProgress);
		transform.translation = maths::lerp(resetPos, Vector3F(-256.0f, 0.0f, 0.0f), progressSmoothed);
		transform.orientation = QuaternionF::slerp(resetOrientation, QuaternionF::identity, progressSmoothed);
		
		if (maths::nearlyEquals(resettingPosOrientationProgress, 1.0f, maths::constants::kindaSmallNumber) || resettingPosOrientationProgress > 1.0f) {
			resettingPosOrientation = false;
			resettingPosOrientationProgress = 0.0f;
			yaw = 0.0f;
			pitch = 0.0f;
			roll = 0.0f;
		}
	}
}

input::InputComponent* PlayerPawn::getInputComponent() {
	return inputComponent;
}

CameraComponent* PlayerPawn::getCameraComponent() {
	return cameraComponent;
}

