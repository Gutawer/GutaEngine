#pragma once
#include "fmt/ostream.h"
#include <cmath>

template <typename T>
const Quaternion<T> Quaternion<T>::identity(0, 0, 0, 1);

template <typename T>
Quaternion<T>& Quaternion<T>::operator+=(const Quaternion<T>& rhs) {
	*this = Quaternion(x + rhs.x, y + rhs.y, z + rhs.z, w + rhs.w);
	return *this;
}

///
/// \relates Quaternion
/// 
/// \brief Adds two quaternions together.
///
/// Adds two quaternions together, adding each component.
///
/// \param lhs The first quaternion to add
/// \param rhs The second quaternion to add
///
/// \return A quaternion equal to lhs + rhs
///
template <typename T>
Quaternion<T> operator+(Quaternion<T> lhs, const Quaternion<T>& rhs) {
	lhs += rhs;
	return lhs;
}

template<typename T>
Quaternion<T>& Quaternion<T>::operator-=(const Quaternion<T>& rhs) {
	*this = Quaternion(x - rhs.x, y - rhs.y, z - rhs.z, w - rhs.w);
	return *this;
}

///
/// \relates Quaternion
/// 
/// \brief Subtracts one quaternion from another.
///
/// Subtracts one quaternion from another, subtracting each component.
///
/// \param lhs The first quaternion to subtract
/// \param rhs The second quaternion to subtract
///
/// \return A quaternion equal to lhs - rhs
///
template <typename T>
Quaternion<T> operator-(Quaternion<T> lhs, const Quaternion<T>& rhs) {
	lhs -= rhs;
	return lhs;
}

template <typename T>
Quaternion<T> Quaternion<T>::operator-() {
	return Quaternion<T>(-x, -y, -z, -w);
}

template <typename T>
Quaternion<T>& Quaternion<T>::operator*=(const Quaternion<T>& rhs) {
	*this = Quaternion(
		 x * rhs.w + y * rhs.z - z * rhs.y + w * rhs.x,
		-x * rhs.z + y * rhs.w + z * rhs.x + w * rhs.y,
		 x * rhs.y - y * rhs.x + z * rhs.w + w * rhs.z,
		-x * rhs.x - y * rhs.y - z * rhs.z + w * rhs.w
	);
	return *this;
}

///
/// \relates Quaternion
/// 
/// \brief Multiplies two quaternions together.
///
/// Multiplies two quaternions together, creating a concatenated rotation.
///
/// \param lhs The first quaternion to multiply
/// \param rhs The second quaternion to multiply
///
/// \return A quaternion equal to lhs * rhs
///
template <typename T>
Quaternion<T> operator*(Quaternion<T> lhs, const Quaternion<T>& rhs) {
	lhs *= rhs;
	return lhs;
}

template <typename T>
Quaternion<T>& Quaternion<T>::operator*=(T rhs) {
	*this = Quaternion<T>(x * rhs, y * rhs, z * rhs, w * rhs);
	return *this;
}

///
/// \relates Quaternion
/// 
/// \brief Multiplies a quaternion and a scalar together.
///
/// Multiplies a quaternion and a scalar together, creating a scaled quaternion.
///
/// \param lhs The quaternion to multiply
/// \param rhs The scalar to multiply
///
/// \return A quaternion equal to lhs * rhs
///
template <typename T>
Quaternion<T> operator*(Quaternion<T> lhs, T rhs) {
	lhs *= rhs;
	return lhs;
}

///
/// \relates Quaternion
/// 
/// \brief Multiplies a quaternion and a scalar together.
///
/// Multiplies a quaternion and a scalar together, creating a scaled quaternion.
///
/// \param lhs The scalar to multiply
/// \param rhs The quaternion to multiply
///
/// \return A quaternion equal to lhs * rhs
///
template <typename T>
Quaternion<T> operator*(T lhs, Quaternion<T> rhs) {
	return rhs * lhs;
}

template <typename T>
Vector3<T> Quaternion<T>::operator*(const Vector3<T>& rhs) const {
	const Vector3<T> t = 2.0f * vec.cross(rhs);
	const Vector3<T> ret = rhs + (w * t) + vec.cross(t);
	return ret;
}

template <typename T>
bool Quaternion<T>::operator==(const Quaternion<T>& rhs) const {
	return (x == rhs.x && y == rhs.y && z == rhs.z && w = rhs.w);
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const Quaternion<T>& quat) {
	return os << quat.toString();
}

template <typename T>
bool Quaternion<T>::nearlyEquals(const Quaternion<T>& rhs, T tolerance) const {
	return maths::nearlyEquals(x, rhs.x, tolerance) && maths::nearlyEquals(y, rhs.y, tolerance) &&
		maths::nearlyEquals(z, rhs.z, tolerance) && maths::nearlyEquals(w, rhs.w, tolerance);
}

template <typename T>
T Quaternion<T>::lengthSquared() const {
	return x * x + y * y + z * z + w * w;
}

template <typename T>
T Quaternion<T>::length() const {
	return std::sqrt(lengthSquared());
}

template <typename T>
Quaternion<T> Quaternion<T>::unit() const {
	return *this / std::sqrt(lengthSquared());
}

template <>
inline Quaternion<float> Quaternion<float>::unit() const {
	return *this * maths::invSqrt(lengthSquared());
}

template<typename T>
Quaternion<T> Quaternion<T>::conjugate() const {
	return Quaternion<T>(-x, -y, -z, w);
}

template<typename T>
Quaternion<T> Quaternion<T>::inverse() const {
	return conjugate() / lengthSquared();
}

template<typename T>
Vector3<T> Quaternion<T>::getForwardVector() const {
	return *this * Vector3<T>::i;
}

template<typename T>
Vector3<T> Quaternion<T>::getRightVector() const {
	return *this * -Vector3<T>::j;
}

template<typename T>
Vector3<T> Quaternion<T>::getUpVector() const {
	return *this * Vector3<T>::k;
}

template<typename T>
Matrix4x4<T> Quaternion<T>::toMatrix() const {
	return Matrix4x4<T>(
		{ 1 - 2 * y * y - 2 * z * z, 2 * x * y + 2 * w * z    , 2 * x * z - 2 * w * y    , 0.0f,
		  2 * x * y - 2 * w * z    , 1 - 2 * x * x - 2 * z * z, 2 * y * z + 2 * w * x    , 0.0f,
		  2 * x * z + 2 * w * y    , 2 * y * z - 2 * w * x    , 1 - 2 * x * x - 2 * y * y, 0.0f,
		  0.0f                     , 0.0f                     , 0.0f                     , 1.0f }
	);
}

template <typename T>
constexpr Quaternion<T> Quaternion<T>::axisAngle(Vector3<T> axis, T angle) {
	T halfAng = angle / 2;
	T s = std::sin(halfAng);
	T c = std::cos(halfAng);
	return Quaternion<T>(axis.x * s, axis.y * s, axis.z * s, c);
}

template <typename T>
constexpr Quaternion<T> Quaternion<T>::yawPitchRoll(T yaw, T pitch, T roll) {
	return axisAngle(Vector3<T>::k, yaw) * axisAngle(-Vector3<T>::j, pitch) * axisAngle(Vector3<T>::i, roll);
}

template <typename T>
Quaternion<T> Quaternion<T>::slerp(Quaternion<T> a, Quaternion<T> b, T alpha) {
	T dot = a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;

	if (dot < 0) {
		b = -b;
		dot = -dot;
	}

	if (dot > static_cast<T>(0.9995)) return maths::lerp(a, b, alpha).unit();

	T omega = std::acos(dot);
	T omegaScaled = omega * alpha;

	T sinOmega = std::sin(omega);
	T sinOmegaScaled = std::sin(omegaScaled);
	T invSinOmega = 1 / sinOmega;

	T scale0 = cos(omegaScaled) - dot * sinOmegaScaled * invSinOmega;
	T scale1 = sinOmegaScaled * invSinOmega;

	return (scale0 * a) + (scale1 * b);
}

template <typename T>
std::string Quaternion<T>::toString() const {
	return "(" + std::to_string(w) + " + " + std::to_string(x) + "i + " + std::to_string(y) + "j + " + std::to_string(w) + "k)";
}

namespace std {
	template <typename T>
	struct hash<Quaternion<T>> {
		std::size_t operator()(const Quaternion<T>& k) const {
			std::size_t ret = 0;
			hashCombine(ret, k.x);
			hashCombine(ret, k.y);
			hashCombine(ret, k.z);
			hashCombine(ret, k.w);
			return ret;
		}
	};
}