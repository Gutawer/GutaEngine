#pragma once
#include "component.hpp"
#include "maths/vector/vector.hpp"
#include "maths/quaternion.hpp"
#include <optional>

template <typename T>
class Transform {
public:
	Quaternion<T> orientation = Quaternion<T>::identity;
	Vector3<T> translation;
	Vector3<T> scale = Vector3F(1.0f, 1.0f, 1.0f);

	Matrix4x4<T> toMatrix() const;

	Transform<T>& operator*=(const Transform<T>& rhs);
};

using TransformF = Transform<float>;
using TransformD = Transform<double>;

#include "transform.inl"