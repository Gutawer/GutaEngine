#pragma once
#include "maths/maths.hpp"
#include <array>
#include <string>

template <typename T>
class Vector3;

template <typename T>
class Matrix4x4;

/// A quaternion class for rotations.
template <typename T>
class Quaternion {
public:
	union {
		std::array<T, 4> vals;

		Vector3<T> vec;

		struct {
			/// The x component of this quaternion.
			T x;
			/// The y component of this quaternion.
			T y;
			/// The z component of this quaternion.
			T z;
			/// The w component of this quaternion.
			T w;
		};
	};

	static const Quaternion<T> identity;

	///
	/// \brief Creates a 0 quaternion.
	/// 
	/// Creates a 0 quaternion.
	///
	Quaternion() : x(0), y(0), z(0), w(0) {}

	///
	/// \brief Creates a quaternion with given components.
	///
	/// Creates a quaternion, setting x, y, z and w to the parameters.
	///
	/// \param x The x component to set
	/// \param y The y component to set
	/// \param z The z component to set
	/// \param w The w component to set
	///
	Quaternion(T x, T y, T z, T w) : x(x), y(y), z(z), w(w) {}

	///
	/// \brief Creates a quaternion from another quaternion.
	///
	/// \param q The quaternion to copy from
	///
	Quaternion(const Quaternion<T>& q) : x(q.x), y(q.y), z(q.z), w(q.w) {}
	
	///
	/// \brief Adds a quaternion to this one.
	/// 
	/// Adds a quaternion to this one, adding each component.
	///
	/// \param rhs The quaternion to add
	///
	/// \return A reference to this quaternion
	///
	Quaternion<T>& operator+=(const Quaternion<T>& rhs);

	///
	/// \brief Substracts a quaternion from this one.
	/// 
	/// Substracts a quaternion from this one, subtracting each component.
	///
	/// \param rhs The quaternion to subtract
	///
	/// \return A reference to this quaternion
	///
	Quaternion<T>& operator-=(const Quaternion<T>& rhs);

	///
	/// \brief Returns this quaternion, but negated.
	/// 
	/// Returns this quaternion, but with each component negated.
	///
	/// \return The negated quaternion
	///
	Quaternion<T> operator-();

	///
	/// \brief Multiplies this quaternion by another one.
	/// 
	/// Multiplies this quaternion by another one, performing rotation concatenation.
	///
	/// \param rhs The quaternion to multiply
	///
	/// \return A reference to this quaternion
	///
	Quaternion<T>& operator*=(const Quaternion<T>& rhs);

	///
	/// \brief Multiplies this quaternion by a scalar.
	/// 
	/// Multiplies this quaternion by a scalar, multiplying each component.
	///
	/// \param rhs The scalar to multiply by
	///
	/// \return A reference to this quaternion
	///
	Quaternion<T>& operator*=(T rhs);

	///
	/// \brief Multiplies this quaternion by a vector.
	/// 
	/// Multiplies this quaternion by a vector, performing rotation by this quaternion.
	///
	/// \param rhs The vector to multiply by
	///
	/// \return The rotated vector
	///
	Vector3<T> operator*(const Vector3<T>& rhs) const;

	///
	/// \brief Checks if two quaternions are exactly equal.
	/// 
	/// Checks if two quaternions are exactly equal, not accounting for floating point errors.
	///
	/// \param rhs The quaternion to check equality for against this
	///
	/// \return Whether the quaternions are exactly equal
	///
	bool operator==(const Quaternion<T>& rhs) const;

	///
	/// \brief Checks if two quaternions are nearly equal.
	/// 
	/// Checks if two quaternions are nearly equal, checking if each of their components are within a certain tolerance.
	///
	/// \param rhs       The quaternion to check near equality for against this
	/// \param tolerance The tolerance to allow for equality checks
	///
	/// \return Whether the quaternions are nearly equal
	///
	bool nearlyEquals(const Quaternion<T>& rhs, T tolerance = maths::constants::kindaSmallNumber) const;

	///
	/// \brief Computes the length squared of this quaternion.
	///
	/// Computes the length squared of this quaternion.
	///
	/// \return The length squared of this quaternion.
	///
	T lengthSquared() const;

	///
	/// \brief Computes the length of this quaternion.
	///
	/// Computes the length of this quaternion.
	///
	/// \return The length of this quaternion.
	///
	T length() const;

	///
	/// \brief Computes this quaternion's unit quaternion.
	///
	/// Computes this quaternion's unit quaternion.
	/// 
	/// \return This quaternion's corresponding unit quaternion.
	///
	Quaternion<T> unit() const;

	///
	/// \brief Computes this quaternion's conjugate.
	///
	/// Computes this quaternion's conjugate.
	/// 
	/// \return This quaternion's conjugate.
	///
	Quaternion<T> conjugate() const;

	///
	/// \brief Computes this quaternion's inverse.
	///
	/// Computes this quaternion's inverse.
	/// 
	/// \return This quaternion's inverse.
	///
	Quaternion<T> inverse() const;

	///
	/// \brief Gets the right direction after it has been rotated by this quaternion.
	///
	/// Gets the right direction after it has been rotated by this quaternion (equal to this * i).
	///
	/// \return The right direction from this quaternion
	///
	Vector3<T> getRightVector() const;

	///
	/// \brief Gets the forward direction after it has been rotated by this quaternion.
	///
	/// Gets the forward direction after it has been rotated by this quaternion (equal to this * j).
	///
	/// \return The forward direction from this quaternion
	///
	Vector3<T> getForwardVector() const;

	///
	/// \brief Gets the up direction after it has been rotated by this quaternion.
	///
	/// Gets the up direction after it has been rotated by this quaternion (equal to this * k).
	///
	/// \return The up direction from this quaternion
	///
	Vector3<T> getUpVector() const;

	///
	/// \brief Gets the equivalent matrix to this quaternion.
	///
	/// Gets the matrix that performs the same rotation as this quaternion.
	///
	/// \return The matrix from this quaternion
	///
	Matrix4x4<T> toMatrix() const;

	///
	/// \brief Creates a quaternion from an axis and an angle.
	///
	/// Creates a quaternion from an axis and an angle.
	///
	/// \param axis  The axis to rotate around
	/// \param angle The angle to rotate by, in radians
	///
	/// \return The quaternion representing the rotation
	///
	constexpr static Quaternion<T> axisAngle(Vector3<T> axis, T angle);

	///
	/// \brief Creates a quaternion from yaw, pitch and roll.
	///
	/// Creates a quaternion representing a rotation by yaw around k, a rotation by pitch around i, and a rotation by roll around j.
	///
	/// \param yaw   The yaw, in radians
	/// \param pitch The pitch, in radians
	/// \param roll  The roll, in radians
	///
	/// \return The quaternion representing the rotation
	///
	constexpr static Quaternion<T> yawPitchRoll(T yaw, T pitch, T roll);

	///
	/// \brief Spherically interpolates between a and b by alpha.
	///
	/// Spherically interpolates between a and b by alpha.
	///
	/// \param a     The starting value
	/// \param b     The ending value
	/// \param alpha How far along from a to b
	///
	/// \return The interpolated value
	///
	static Quaternion<T> slerp(Quaternion<T> a, Quaternion<T> b, T alpha);

	///
	/// \brief Creates a readable string from this quaternion.
	///
	/// Creates a readable string from this quaternion, in the form "w + xi + yj + zk".
	///
	/// \return The readable string.
	///
	std::string toString() const;
};

using QuaternionF = Quaternion<float>;
using QuaternionD = Quaternion<double>;

#include "quaternion.inl"