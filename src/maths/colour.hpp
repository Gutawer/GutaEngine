#pragma once

class Colour {
public:
	float r = 0.0f;
	float g = 0.0f;
	float b = 0.0f;
	float a = 0.0f;

	Colour() {}
	Colour(float r, float g, float b, float a = 1.0f) : r(r), g(g), b(b), a(a) {}
};