#include "physics_mesh.hpp"
#include "obj_mesh_loader.hpp"

typedef enum test { one, two } Test;

std::unordered_map<std::filesystem::path, std::shared_ptr<PhysicsMesh>, filesystem::PathHash, filesystem::PathEquality> PhysicsMesh::meshes;

std::shared_ptr<PhysicsMesh> PhysicsMesh::getMesh(filesystem::ResourceLocation path) {
	auto iter = meshes.find(path.getAbsolutePath());
	if (iter == meshes.end()) {
		std::shared_ptr<PhysicsMesh> mesh = meshloaders::physics::loadObj(path);
		meshes[path.getAbsolutePath()] = mesh;
		return mesh;
	}
	else return iter->second;
}

void PhysicsMesh::addMomentOfInertia(float density) {
}
