#pragma once
#include "hash.hpp"
#include "fmt/ostream.h"

template <typename T>
const Vector4<T> Vector4<T>::i = Vector4<T>(1, 0, 0, 0);

template <typename T>
const Vector4<T> Vector4<T>::j = Vector4<T>(0, 1, 0, 0);

template <typename T>
const Vector4<T> Vector4<T>::k = Vector4<T>(0, 0, 1, 0);

template <typename T>
const Vector4<T> Vector4<T>::l = Vector4<T>(0, 0, 0, 1);

template <typename T>
Vector4<T>& Vector4<T>::operator+=(const Vector4<T>& rhs) {
	*this = Vector4(x + rhs.x, y + rhs.y, z + rhs.z, w + rhs.w);
	return *this;
}

///
/// \relates Vector4
/// 
/// \brief Adds two vectors together.
///
/// Adds two vectors together, adding each component.
///
/// \param lhs The first vector to add
/// \param rhs The second vector to add
///
/// \return A vector equal to lhs + rhs
///
template <typename T>
Vector4<T> operator+(Vector4<T> lhs, const Vector4<T>& rhs) {
	lhs += rhs;
	return lhs;
}

template <typename T>
Vector4<T>& Vector4<T>::operator-=(const Vector4<T>& rhs) {
	*this = Vector4(x - rhs.x, y - rhs.y, z - rhs.z, w - rhs.w);
	return *this;
}

///
/// \relates Vector4
/// 
/// \brief Subtracts one vector from another.
///
/// Subtracts one vector from another, subtracting each component.
///
/// \param lhs The first vector to subtract from
/// \param rhs The second vector to subtract by
///
/// \return A vector equal to lhs - rhs
///
template <typename T>
Vector4<T> operator-(Vector4<T> lhs, const Vector4<T>& rhs) {
	lhs -= rhs;
	return lhs;
}

template <typename T>
Vector4<T> Vector4<T>::operator-() const {
	return Vector4(-x, -y, -z);
}

template <typename T>
Vector4<T>& Vector4<T>::operator*=(T rhs) {
	*this = Vector4<T>(x * rhs, y * rhs, z * rhs, w * rhs);
	return *this;
}

///
/// \relates Vector4
/// 
/// \brief Multiplies a vector by a scalar.
///
/// Multiplies a vector by a scalar, multiplying each component.
///
/// \param lhs The vector to multiply
/// \param rhs The scalar to multiply by
///
/// \return A vector equal to lhs * rhs
///
template <typename T>
Vector4<T> operator*(Vector4<T> lhs, T rhs) {
	lhs *= rhs;
	return lhs;
}

///
/// \relates Vector4
/// 
/// \brief Multiplies a vector by a scalar.
///
/// Multiplies a vector by a scalar, multiplying each component.
///
/// \param lhs The scalar to multiply
/// \param rhs The vector to multiply by
///
/// \return A vector equal to lhs * rhs
///
template <typename T>
Vector4<T> operator*(T lhs, const Vector4<T>& rhs) {
	return rhs * lhs;
}

template <typename T>
Vector4<T>& Vector4<T>::operator/=(T rhs) {
	*this = Vector4<T>(x / rhs, y / rhs, z / rhs, w / rhs);
	return *this;
}

///
/// \relates Vector4
/// 
/// \brief Divides a vector by a scalar.
///
/// Multiplies a vector by a scalar, dividing each component.
///
/// \param lhs The vector to divide
/// \param rhs The scalar to divide by
///
/// \return A vector equal to lhs / rhs
///
template <typename T>
Vector4<T> operator/(Vector4<T> lhs, T rhs) {
	lhs /= rhs;
	return lhs;
}

template <typename T>
bool Vector4<T>::operator==(const Vector4<T>& rhs) const {
	return (x == rhs.x && y == rhs.y && z == rhs.z && w = rhs.w);
}

template<typename T>
inline T& Vector4<T>::operator[](size_t i) {
	return vals[i];
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const Vector4<T>& vec) {
	return os << vec.toString();
}

template <typename T>
bool Vector4<T>::nearlyEquals(const Vector4<T>& rhs, T tolerance) {
	return maths::nearlyEquals(x, rhs.x, tolerance) && maths::nearlyEquals(y, rhs.y, tolerance) &&
		maths::nearlyEquals(z, rhs.z, tolerance) && maths::nearlyEquals(w, rhs.w, tolerance);
}

template <typename T>
T Vector4<T>::dot(const Vector4<T>& rhs) const {
	return x * rhs.x + y * rhs.y + z * rhs.z + w * rhs.w;
}

template <typename T>
T Vector4<T>::lengthSquared() const {
	return x * x + y * y + z * z + w * w;
}

template <typename T>
T Vector4<T>::length() const {
	return std::sqrt(lengthSquared());
}

template <typename T>
Vector4<T> Vector4<T>::unit() const {
	return *this / std::sqrt(lengthSquared());
}

template <>
inline Vector4<float> Vector4<float>::unit() const {
	return *this * maths::invSqrt(lengthSquared());
}

template<typename T>
T Vector4<T>::lengthSquared3() const {
	return x * x + y * y + z * z;
}

template<typename T>
T Vector4<T>::length3() const {
	return std::sqrt(lengthSquared3());
}

template <typename T>
Vector4<T> Vector4<T>::unit3() const {
	return Vector4<T>(Vector3<T>(x, y, z) / std::sqrt(lengthSquared3()), w);
}

template <>
inline Vector4<float> Vector4<float>::unit3() const {
	return Vector4<float>(Vector3<float>(x, y, z) * maths::invSqrt(lengthSquared3()), w);
}

template <typename T>
std::string Vector4<T>::toString() const {
	return "(" + std::to_string(x) + ", " + std::to_string(y) + ", " + std::to_string(z) + ", " + std::to_string(w) + ")";
}

namespace std {
	template <typename T>
	struct hash<Vector4<T>> {
		std::size_t operator()(const Vector4<T>& k) const {
			std::size_t ret = 0;
			hashCombine(ret, k.x);
			hashCombine(ret, k.y);
			hashCombine(ret, k.z);
			hashCombine(ret, k.w);
			return ret;
		}
	};
}