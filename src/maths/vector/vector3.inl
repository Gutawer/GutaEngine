#pragma once
#include "hash.hpp"
#include "fmt/ostream.h"

template <typename T>
const Vector3<T> Vector3<T>::i = Vector3<T>(1, 0, 0);

template <typename T>
const Vector3<T> Vector3<T>::j = Vector3<T>(0, 1, 0);

template <typename T>
const Vector3<T> Vector3<T>::k = Vector3<T>(0, 0, 1);

template <typename T>
Vector3<T>& Vector3<T>::operator+=(const Vector3<T>& rhs) {
	*this = Vector3(x + rhs.x, y + rhs.y, z + rhs.z);
	return *this;
}

///
/// \relates Vector3
/// 
/// \brief Adds two vectors together.
///
/// Adds two vectors together, adding each component.
///
/// \param lhs The first vector to add
/// \param rhs The second vector to add
///
/// \return A vector equal to lhs + rhs
///
template <typename T>
Vector3<T> operator+(Vector3<T> lhs, const Vector3<T>& rhs) {
	lhs += rhs;
	return lhs;
}

template <typename T>
Vector3<T>& Vector3<T>::operator-=(const Vector3<T>& rhs) {
	*this = Vector3(x - rhs.x, y - rhs.y, z - rhs.z);
	return *this;
}

///
/// \relates Vector3
/// 
/// \brief Subtracts one vector from another.
///
/// Subtracts one vector from another, subtracting each component.
///
/// \param lhs The first vector to subtract from
/// \param rhs The second vector to subtract by
///
/// \return A vector equal to lhs - rhs
///
template <typename T>
Vector3<T> operator-(Vector3<T> lhs, const Vector3<T>& rhs) {
	lhs -= rhs;
	return lhs;
}

template <typename T>
Vector3<T> Vector3<T>::operator-() const {
	return Vector3(-x, -y, -z);
}

template <typename T>
Vector3<T>& Vector3<T>::operator*=(T rhs) {
	*this = Vector3<T>(x * rhs, y * rhs, z * rhs);
	return *this;
}

///
/// \relates Vector3
/// 
/// \brief Multiplies a vector by a scalar.
///
/// Multiplies a vector by a scalar, multiplying each component.
///
/// \param lhs The vector to multiply
/// \param rhs The scalar to multiply by
///
/// \return A vector equal to lhs * rhs
///
template <typename T>
Vector3<T> operator*(Vector3<T> lhs, T rhs) {
	lhs *= rhs;
	return lhs;
}

///
/// \relates Vector3
/// 
/// \brief Multiplies a vector by a scalar.
///
/// Multiplies a vector by a scalar, multiplying each component.
///
/// \param lhs The scalar to multiply
/// \param rhs The vector to multiply by
///
/// \return A vector equal to lhs * rhs
///
template <typename T>
Vector3<T> operator*(T lhs, const Vector3<T>& rhs) {
	return rhs * lhs;
}

template <typename T>
Vector3<T>& Vector3<T>::operator/=(T rhs) {
	*this = Vector3<T>(x / rhs, y / rhs, z / rhs);
	return *this;
}

///
/// \relates Vector3
/// 
/// \brief Divides a vector by a scalar.
///
/// Multiplies a vector by a scalar, dividing each component.
///
/// \param lhs The vector to divide
/// \param rhs The scalar to divide by
///
/// \return A vector equal to lhs / rhs
///
template <typename T>
Vector3<T> operator/(Vector3<T> lhs, T rhs) {
	lhs /= rhs;
	return lhs;
}

template <typename T>
bool Vector3<T>::operator==(const Vector3<T>& rhs) const {
	return (x == rhs.x && y == rhs.y && z == rhs.z);
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const Vector3<T>& vec) {
	return os << vec.toString();
}

template<typename T>
inline T& Vector3<T>::operator[](size_t i) {
	return vals[i];
}

template <typename T>
bool Vector3<T>::nearlyEquals(const Vector3<T>& rhs, T tolerance) {
	return maths::nearlyEquals(x, rhs.x, tolerance) && maths::nearlyEquals(y, rhs.y, tolerance) && maths::nearlyEquals(z, rhs.z, tolerance);
}

template <typename T>
T Vector3<T>::dot(const Vector3<T>& rhs) const {
	return x * rhs.x + y * rhs.y + z * rhs.z;
}

template <typename T>
Vector3<T> Vector3<T>::cross(const Vector3<T>& rhs) const {
	return Vector3<T>(
		y * rhs.z - z * rhs.y,
		z * rhs.x - x * rhs.z,
		x * rhs.y - y * rhs.x
		);
}

template <typename T>
T Vector3<T>::lengthSquared() const {
	return x * x + y * y + z * z;
}

template <typename T>
T Vector3<T>::length() const {
	return std::sqrt(lengthSquared());
}

template <typename T>
Vector3<T> Vector3<T>::unit() const {
	return *this / std::sqrt(lengthSquared());
}

template <>
inline Vector3<float> Vector3<float>::unit() const {
	return *this * maths::invSqrt(lengthSquared());
}

template <typename T>
std::string Vector3<T>::toString() const {
	return "(" + std::to_string(x) + ", " + std::to_string(y) + ", " + std::to_string(z) + ")";
}

namespace std {
	template <typename T>
	struct hash<Vector3<T>> {
		std::size_t operator()(const Vector3<T>& k) const {
			std::size_t ret = 0;
			hashCombine(ret, k.x);
			hashCombine(ret, k.y);
			hashCombine(ret, k.z);
			return ret;
		}
	};
}