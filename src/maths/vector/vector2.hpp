#pragma once
#include <string>
#include <cmath>
#include <array>
#include "maths/maths.hpp"

/// A 2D Vector class.
template <typename T>
class Vector2 {
public:
	union {
		std::array<T, 2> vals;

		struct {
			/// The x component of this vector.
			T x;
			/// The y component of this vector.
			T y;
		};
	};

	/// The i vector.
	static const Vector2<T> i;
	/// The j vector.
	static const Vector2<T> j;
	
	///
	/// \brief Creates a 0 vector.
	/// 
	/// Creates a 0 vector.
	///
	Vector2() : x(0), y(0) {}

	///
	/// \brief Creates a vector with given components.
	///
	/// Creates a vector, setting x and y to the parameters.
	///
	/// \param x The x component to set
	/// \param y The y component to set
	///
	Vector2(T x, T y) : x(x), y(y) {}

	///
	/// \brief Creates a vector from another vector.
	///
	/// \param v The vector to copy from
	///
	Vector2(const Vector2<T>& v) : x(v.x), y(v.y) {}

	///
	/// \brief Adds a vector to this one.
	/// 
	/// Adds a vector to this one, adding each component.
	///
	/// \param rhs The vector to add
	///
	/// \return A reference to this vector
	///
	Vector2<T>& operator+=(const Vector2<T>& rhs);

	///
	/// \brief Subtracts a vector from this one.
	/// 
	/// Subtracts a vector from this one, subtracting each component.
	///
	/// \param rhs The vector to subtract
	///
	/// \return A reference to this vector
	///
	Vector2<T>& operator-=(const Vector2<T>& rhs);

	///
	/// \brief Returns this vector, but negated.
	/// 
	/// Returns this vector, but with each component negated.
	///
	/// \return The negated vector
	///
	Vector2<T> operator-() const;

	///
	/// \brief Multiplies this vector by a scalar.
	/// 
	/// Multiplies this vector by a scalar, by multiplying each component.
	///
	/// \param rhs The scalar to multiply by
	///
	/// \return A reference to this vector
	///
	Vector2<T>& operator*=(T rhs);

	///
	/// \brief Divides this vector by a scalar.
	/// 
	/// Divides this vector by a scalar, by dividing each component.
	///
	/// \param rhs The scalar to divide by
	///
	/// \return A reference to this vector
	///
	Vector2<T>& operator/=(T rhs);

	///
	/// \brief Checks if two vectors are exactly equal.
	/// 
	/// Checks if two vectors are exactly equal, not accounting for floating point errors.
	///
	/// \param rhs The vectors to check equality for against this
	///
	/// \return Whether the vectors are exactly equal
	///
	bool operator==(const Vector2<T>& rhs) const;

	///
	/// \brief Returns the element at the index.
	///
	/// Returns the element in the vector at the specified index.
	///
	/// \param i The index to get
	///
	/// \return A reference to the element
	///
	T& operator[](size_t i);

	///
	/// \brief Checks if two vectors are nearly equal.
	/// 
	/// Checks if two vectors are nearly equal, checking if each of their components are within a certain tolerance.
	///
	/// \param rhs       The vector to check near equality for against this
	/// \param tolerance The tolerance to allow for equality checks
	///
	/// \return Whether the vectors are nearly equal
	///
	bool nearlyEquals(const Vector2<T>& rhs, T tolerance = maths::constants::kindaSmallNumber);

	///
	/// \brief Computes the dot product of this vector and another.
	///
	/// Computes the dot product of this vector and another.
	///
	/// \param rhs The vector to compute the dot product of this with
	///
	/// \return The dot product of this and rhs
	///
	T dot(const Vector2<T>& rhs) const;

	///
	/// \brief Computes the length squared of this vector.
	///
	/// Computes the length squared of this vector. Useful for vector length comparisons as it is cheaper to compute than length().
	///
	/// \return The length squared of this vector.
	///
	T lengthSquared() const;

	///
	/// \brief Computes the length of this vector.
	///
	/// Computes the length of this vector. For when actual vector length is needed - lengthSquared() will be faster for length comparisons.
	///
	/// \return The length of this vector.
	///
	T length() const;

	///
	/// \brief Computes the unit vector of this one.
	///
	/// Computes the vector pointing in the same direction as this one, but with magnitude of 1.
	/// 
	/// \return This vector's corresponding unit vector.
	///
	Vector2<T> unit() const;

	///
	/// \brief Computes a rotated vector.
	///
	/// Computes the result of rotating this vector counter-clockwise around origin.
	///
	/// \param ang    The angle to rotate by, in radians
	/// \param origin The point to rotate around.
	///
	/// \return The rotated vector.
	///
	Vector2<T> rotate(T ang, Vector2<T> origin = Vector2<T>());

	///
	/// \brief Creates a readable string from this vector.
	///
	/// Creates a readable string from this quaternion, in the form "(x, y)".
	///
	/// \return The readable string.
	///
	std::string toString() const;
};

using Vector2I = Vector2<int>;
using Vector2F = Vector2<float>;
using Vector2D = Vector2<double>;

#include "vector2.inl"