#pragma once
#include "hash.hpp"
#include "fmt/ostream.h"

template <typename T>
const Vector2<T> Vector2<T>::i = Vector2<T>(1, 0);

template <typename T>
const Vector2<T> Vector2<T>::j = Vector2<T>(0, 1);

template <typename T>
Vector2<T>& Vector2<T>::operator+=(const Vector2<T>& rhs) {
	*this = Vector2(x + rhs.x, y + rhs.y);
	return *this;
}

///
/// \relates Vector2
/// 
/// \brief Adds two vectors together.
///
/// Adds two vectors together, adding each component.
///
/// \param lhs The first vector to add
/// \param rhs The second vector to add
///
/// \return A vector equal to lhs + rhs
///
template <typename T>
Vector2<T> operator+(Vector2<T> lhs, const Vector2<T>& rhs) {
	lhs += rhs;
	return lhs;
}

template <typename T>
Vector2<T>& Vector2<T>::operator-=(const Vector2<T>& rhs) {
	*this = Vector2(x - rhs.x, y - rhs.y);
	return *this;
}

///
/// \relates Vector2
/// 
/// \brief Subtracts one vector from another.
///
/// Subtracts one vector from another, subtracting each component.
///
/// \param lhs The first vector to subtract from
/// \param rhs The second vector to subtract by
///
/// \return A vector equal to lhs - rhs
///
template <typename T>
Vector2<T> operator-(Vector2<T> lhs, const Vector2<T>& rhs) {
	lhs -= rhs;
	return lhs;
}

template <typename T>
Vector2<T> Vector2<T>::operator-() const {
	return Vector2(-x, -y);
}

template <typename T>
Vector2<T>& Vector2<T>::operator*=(T rhs) {
	*this = Vector2<T>(x * rhs, y * rhs);
	return *this;
}

///
/// \relates Vector2
/// 
/// \brief Multiplies a vector by a scalar.
///
/// Multiplies a vector by a scalar, multiplying each component.
///
/// \param lhs The vector to multiply
/// \param rhs The scalar to multiply by
///
/// \return A vector equal to lhs * rhs
///
template <typename T>
Vector2<T> operator*(Vector2<T> lhs, T rhs) {
	lhs *= rhs;
	return lhs;
}

///
/// \relates Vector2
/// 
/// \brief Multiplies a vector by a scalar.
///
/// Multiplies a vector by a scalar, multiplying each component.
///
/// \param lhs The scalar to multiply
/// \param rhs The vector to multiply by
///
/// \return A vector equal to lhs * rhs
///
template <typename T>
Vector2<T> operator*(T lhs, const Vector2<T>& rhs) {
	return rhs * lhs;
}

template <typename T>
Vector2<T>& Vector2<T>::operator/=(T rhs) {
	*this = Vector2<T>(x / rhs, y / rhs);
	return *this;
}

///
/// \relates Vector2
/// 
/// \brief Divides a vector by a scalar.
///
/// Multiplies a vector by a scalar, dividing each component.
///
/// \param lhs The vector to divide
/// \param rhs The scalar to divide by
///
/// \return A vector equal to lhs / rhs
///
template <typename T>
Vector2<T> operator/(Vector2<T> lhs, T rhs) {
	lhs /= rhs;
	return lhs;
}

template <typename T>
bool Vector2<T>::operator==(const Vector2<T>& rhs) const {
	return (x == rhs.x && y == rhs.y);
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const Vector2<T>& vec) {
	return os << vec.toString();
}

template<typename T>
inline T& Vector2<T>::operator[](size_t i) {
	return vals[i];
}

template <typename T>
bool Vector2<T>::nearlyEquals(const Vector2<T>& rhs, T tolerance) {
	return maths::nearlyEquals(x, rhs.x, tolerance) && maths::nearlyEquals(y, rhs.y, tolerance);
}

template <typename T>
T Vector2<T>::dot(const Vector2<T>& rhs) const {
	return x * rhs.x + y * rhs.y;
}

template <typename T>
T Vector2<T>::lengthSquared() const {
	return x * x + y * y;
}

template <typename T>
T Vector2<T>::length() const {
	return std::sqrt(lengthSquared());
}

template <typename T>
Vector2<T> Vector2<T>::unit() const {
	return *this / std::sqrt(lengthSquared());
}

template<typename T>
Vector2<T> Vector2<T>::rotate(T ang, Vector2<T> origin) {
	T c = std::cos(ang);
	T s = std::sin(ang);
	Vector2<T> temp = *this - origin;
	temp = Vector2<T>(c * temp.x - s * temp.y, s * temp.x + c * temp.y);
	temp += origin;
	return temp;
}

template <>
inline Vector2<float> Vector2<float>::unit() const {
	return *this * maths::invSqrt(lengthSquared());
}

template <typename T>
std::string Vector2<T>::toString() const {
	return "(" + std::to_string(x) + ", " + std::to_string(y) + ")";
}

namespace std {
	template <typename T>
	struct hash<Vector2<T>> {
		std::size_t operator()(const Vector2<T>& k) const {
			std::size_t ret = 0;
			hashCombine(ret, k.x);
			hashCombine(ret, k.y);
			return ret;
		}
	};
}