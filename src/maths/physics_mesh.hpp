#pragma once
#include "maths/vector/vector.hpp"
#include "maths/matrix/matrix.hpp"
#include "file.hpp"
#include <vector>

class PhysicsMesh {
public:
	PhysicsMesh(std::vector<std::array<uint32_t, 3>> triangles, std::vector<Vector3F> verts) : triangles(triangles), verts(verts) {
		addMomentOfInertia(1.0f);
	}

	std::vector<std::array<uint32_t, 3>> triangles;
	std::vector<Vector3F> verts;

	Matrix3x3F momentOfInertia;

	static std::shared_ptr<PhysicsMesh> getMesh(filesystem::ResourceLocation path);
private:
	static std::unordered_map<std::filesystem::path, std::shared_ptr<PhysicsMesh>, filesystem::PathHash, filesystem::PathEquality> meshes;

	void addMomentOfInertia(float density);
};