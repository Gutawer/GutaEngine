#pragma once
#include <array>

template <typename T>
class Vector2;

template <typename T>
class Vector3;

/// A matrix class for 2D affine transformations.
template <typename T>
class Matrix3x3 {
public:
	/// The elements in this matrix.
	mutable std::array<T, 9> values{ 0 };

	/// The identity matrix.
	static const Matrix3x3<T> identity;

	///
	/// \brief Creates a 0 matrix.
	///
	/// Creates a 0 matrix.
	///
	Matrix3x3<T>() : values{ 0 } {}

	///
	/// \brief Creates a matrix from an array of values.
	/// 
	/// Creates a matrix from an array of values, laid out in column-major order (like OpenGL).
	///
	/// \param arr The array to copy from.
	///
	Matrix3x3<T>(const std::array<T, 9>& arr) : values(arr) {}

	///
	/// \brief Creates a matrix from 3 vectors.
	///
	/// Creates a matrix from 3 vectors, where each column of the matrix is one of the vectors.
	///
	/// \param v0 The first column vector
	/// \param v1 The second column vector
	/// \param v2 The third column vector
	///
	Matrix3x3<T>(const Vector3<T>& v0, const Vector3<T>& v1, const Vector3<T>& v2);

	///
	/// \brief Accesses the value at a row and column of this matrix.
	///
	/// Accesses the value at a row and column of this matrix.
	///
	/// \param row The row to index from
	/// \param col The column to index from
	///
	/// \return The value at the row and column
	///
	inline T& operator()(int row, int col) const;
	
	///
	/// \brief Adds a matrix to this one.
	/// 
	/// Adds a matrix to this one, adding each element.
	///
	/// \param rhs The matrix to add
	///
	/// \return A reference to this matrix
	///
	Matrix3x3<T>& operator+=(const Matrix3x3<T>& rhs);

	///
	/// \brief Subtracts a matrix from this one.
	/// 
	/// Subtracts a matrix from this one, subtracting each element.
	///
	/// \param rhs The matrix to subtract
	///
	/// \return A reference to this matrix
	///
	Matrix3x3<T>& operator-=(const Matrix3x3<T>& rhs);

	///
	/// \brief Returns this matrix, but negated.
	/// 
	/// Returns this matrix, but with each element negated.
	///
	/// \return The negated matrix
	///
	Matrix3x3<T> operator-() const;

	///
	/// \brief Multiplies this matrix by a scalar.
	/// 
	/// Multiplies this matrix by a scalar, by multiplying each element.
	///
	/// \param rhs The scalar to multiply by
	///
	/// \return A reference to this matrix
	///
	Matrix3x3<T>& operator*=(T rhs);

	///
	/// \brief Multiplies this matrix by a matrix.
	/// 
	/// Multiplies this matrix by a matrix, performing transformation concatenation.
	///
	/// \param rhs The matrix to multiply by
	///
	/// \return A reference to this matrix
	///
	Matrix3x3<T>& operator*=(const Matrix3x3<T>& rhs);

	///
	/// \brief Multiplies this matrix by a 3D column vector.
	/// 
	/// Multiplies this matrix by a 3D column vector, applying a transformation.
	///
	/// \param rhs The vector to multiply by
	///
	/// \return The transformed vector
	///
	Vector3<T> operator*(const Vector3<T>& rhs) const;

	///
	/// \brief Multiplies this matrix by a 2D column vector.
	/// 
	/// Multiplies this matrix by a 2D column vector, turned internally into a 3D column vector with z component of 1, applying a transformation.
	///
	/// \param rhs The vector to multiply by
	///
	/// \return The transformed (3D) vector
	///
	Vector3<T> operator*(const Vector2<T>& rhs) const;

	///
	/// \brief Checks if two matrices are exactly equal.
	/// 
	/// Checks if two matrices are exactly equal, not accounting for floating point errors.
	///
	/// \param rhs The matrices to check equality for against this
	///
	/// \return Whether the matrices are exactly equal
	///
	bool operator==(const Matrix3x3<T>& rhs) const;

	///
	/// \brief Checks if two matrices are nearly equal.
	/// 
	/// Checks if two matrices are nearly equal, checking if each of their elements are within a certain tolerance.
	///
	/// \param rhs       The matrix to check near equality for against this
	/// \param tolerance The tolerance to allow for equality checks
	///
	/// \return Whether the matrices are nearly equal
	///
	bool nearlyEquals(const Matrix3x3<T>& rhs, float tolerance = maths::constants::kindaSmallNumber);

	///
	/// \brief Computes the transpose of this matrix.
	///
	/// Computes the transpose of this matrix.
	///
	/// \return The transpose of this matrix
	///
	Matrix3x3<T> transpose() const;

	///
	/// \brief Computes the inverse of this matrix.
	///
	/// Computes the inverse of this matrix.
	///
	/// \return The inverse of this matrix
	///
	Matrix3x3<T> inverse() const;

	///
	/// \brief Returns a row of this matrix as a Vector3.
	///
	/// Returns a row of this matrix as a Vector3.
	///
	/// \param row The row to create a vector from.
	///
	/// \return The vector from row
	///
	Vector3<T> row(int row) const;

	///
	/// \brief Returns a column of this matrix as a Vector3.
	///
	/// Returns a column of this matrix as a Vector3.
	///
	/// \param col The column to create a vector from.
	///
	/// \return The vector from col
	///
	Vector3<T> col(int col) const;

	///
	/// \brief Creates a 2D rotation matrix.
	///
	/// Creates a matrix representing a 2D rotation counter-clockwise around the origin.
	///
	/// \param angle The angle to rotate by, in radians
	///
	/// \return The rotation matrix
	///
	static Matrix3x3<T> rotate(T angle);

	///
	/// \brief Creates a 2D scale matrix.
	///
	/// Creates a matrix representing a 2D scale about the origin.
	///
	/// \param s The vector to scale by
	///
	/// \return The scale matrix
	///
	static Matrix3x3<T> scale(Vector2<T> s);

	///
	/// \brief Creates a 2D translation matrix.
	///
	/// Creates a matrix representing a 2D translation.
	///
	/// \param t The vector to translate by
	///
	/// \return The translate matrix
	///
	static Matrix3x3<T> translate(Vector2<T> t);

	///
	/// \brief Creates a 2D TRS matrix.
	///
	/// Creates a matrix representing a 2D scale about the origin, followed by a 2D rotation counter-clockwise around the origin, followed by a 2D translation.
	///
	/// \param scaleVec     The vector to scale by
	/// \param angle        The angle to rotate by, in radians
	/// \param translateVec The vector to translate by
	///
	/// \return The TRS matrix
	///
	static Matrix3x3<T> trs(Vector2<T> scaleVec, T angle, Vector2<T> translateVec);

	///
	/// \brief Creates a readable string from this matrix.
	///
	/// Creates a readable string from this matrix, in the form "{ 00, 01, 02\n 10, ... 21, 22 }".
	///
	/// \return The readable string.
	///
	std::string toString() const;
};

using Matrix3x3I = Matrix3x3<int>;
using Matrix3x3F = Matrix3x3<float>;
using Matrix3x3D = Matrix3x3<double>;

#include "matrix3x3.inl"