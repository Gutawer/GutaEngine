#pragma once
#include "hash.hpp"
#include "fmt/ostream.h"
#include <cmath>
#include <algorithm>
#include <string>
#include <iomanip>
#include <sstream>
#include <cassert>
#include "matrix4x4.hpp"

template <typename T>
const Matrix4x4<T> Matrix4x4<T>::identity(
	{ 1, 0, 0, 0,
	  0, 1, 0, 0,
	  0, 0, 1, 0,
	  0, 0, 0, 1 }
);

template <typename T>
const Matrix4x4<T> Matrix4x4<T>::engineCoordinatesToGLWorldCoords(
	{  0,  0, -1,  0,
	  -1,  0,  0,  0,
	   0,  1,  0,  0,
	   0,  0,  0,  1 }
);

template <typename T>
Matrix4x4<T>::Matrix4x4(const Vector4<T>& v0, const Vector4<T>& v1, const Vector4<T>& v2, const Vector4<T>& v3) {
	(*this)(0, 0) = v0.x; (*this)(1, 0) = v0.y; (*this)(2, 0) = v0.z; (*this)(3, 0) = v0.w;
	(*this)(0, 1) = v1.x; (*this)(1, 1) = v1.y; (*this)(2, 1) = v1.z; (*this)(3, 1) = v1.w;
	(*this)(0, 2) = v2.x; (*this)(1, 2) = v2.y; (*this)(2, 2) = v2.z; (*this)(3, 2) = v2.w;
	(*this)(0, 3) = v3.x; (*this)(1, 3) = v3.y; (*this)(2, 3) = v3.z; (*this)(3, 3) = v3.w;
}

template <typename T>
inline T& Matrix4x4<T>::operator()(int row, int col) const {
	assert(row >= 0 && row <= 3 && col >= 0 && col <= 3);
	return values[col * 4 + row];
}

template <typename T>
Matrix4x4<T>& Matrix4x4<T>::operator+=(const Matrix4x4<T>& rhs) {
	for (int r = 0; r < 4; r++) {
		for (int c = 0; c < 4; c++) {
			(*this)(r, c) += rhs(r, c);
		}
	}
	return *this;
}

///
/// \relates Matrix4x4
/// 
/// \brief Adds two matrices together.
///
/// Adds two matrices together, adding each component.
///
/// \param lhs The first matrix to add
/// \param rhs The second matrix to add
///
/// \return A matrix equal to lhs + rhs
///
template <typename T>
Matrix4x4<T> operator+(Matrix4x4<T> lhs, const Matrix4x4<T>& rhs) {
	lhs += rhs;
	return lhs;
}

template <typename T>
Matrix4x4<T>& Matrix4x4<T>::operator-=(const Matrix4x4<T>& rhs) {
	for (int r = 0; r < 4; r++) {
		for (int c = 0; c < 4; c++) {
			(*this)(r, c) -= rhs(r, c);
		}
	}
	return *this;
}

///
/// \relates Matrix4x4
/// 
/// \brief Subtracts one matrix from another.
///
/// Subtracts one matrix from another, subtracting each component.
///
/// \param lhs The first matrix to subtract from
/// \param rhs The second matrix to subtract by
///
/// \return A matrix equal to lhs - rhs
///
template <typename T>
Matrix4x4<T> operator-(Matrix4x4<T> lhs, const Matrix4x4<T>& rhs) {
	lhs -= rhs;
	return lhs;
}

template <typename T>
Matrix4x4<T> Matrix4x4<T>::operator-() const {
	Matrix4x4<T> ret;
	for (int r = 0; r < 4; r++) {
		for (int c = 0; c < 4; c++) {
			ret(r, c) = -(*this)(r, c);
		}
	}
	return ret;
}

template <typename T>
Matrix4x4<T>& Matrix4x4<T>::operator*=(T rhs) {
	for (int i = 0; i < 16; i++) {
		values[i] *= rhs;
	}
	return *this;
}

///
/// \relates Matrix4x4
/// 
/// \brief Multiplies a matrix by a scalar.
///
/// Multiplies a matrix by a scalar, multiplying each component.
///
/// \param lhs The matrix to multiply
/// \param rhs The scalar to multiply by
///
/// \return A matrix equal to lhs * rhs
///
template <typename T>
Matrix4x4<T> operator*(Matrix4x4<T> lhs, T rhs) {
	lhs *= rhs;
	return lhs;
}

///
/// \relates Matrix4x4
/// 
/// \brief Multiplies a matrix by a scalar.
///
/// Multiplies a matrix by a scalar, multiplying each component.
///
/// \param lhs The scalar to multiply by
/// \param rhs The matrix to multiply
///
/// \return A matrix equal to lhs * rhs
///
template <typename T>
Matrix4x4<T> operator*(T lhs, const Matrix4x4<T>& rhs) {
	return rhs * lhs;
}

template <typename T>
Matrix4x4<T>& Matrix4x4<T>::operator*=(const Matrix4x4<T>& rhs) {
	Matrix4x4<T> temp;
	for (int r = 0; r < 4; r++) {
		for (int c = 0; c < 4; c++) {
			temp(r, c) = (*this)(r, 0) * rhs(0, c) + (*this)(r, 1) * rhs(1, c) + (*this)(r, 2) * rhs(2, c) + (*this)(r, 3) * rhs(3, c);
		}
	}
	*this = temp;
	return *this;
}

///
/// \relates Matrix4x4
///
/// \brief Multiplies a matrix by a matrix.
/// 
/// Multiplies a matrix by a matrix, performing transformation concatenation.
///
/// \param lhs The first matrix to multiply
/// \param rhs The second matrix to multiply
///
/// \return A matrix equal to lhs * rhs
///
template <typename T>
Matrix4x4<T> operator*(Matrix4x4<T> lhs, const Matrix4x4<T>& rhs) {
	lhs *= rhs;
	return lhs;
}

template <typename T>
Vector4<T> Matrix4x4<T>::operator*(const Vector4<T>& rhs) const {
	return Vector4<T>(row(0).dot(rhs), row(1).dot(rhs), row(2).dot(rhs), row(3).dot(rhs));
}

template <typename T>
Vector4<T> Matrix4x4<T>::operator*(const Vector3<T>& rhs) const {
	Vector4<T> vec = Vector4<T>(rhs, 1.0f);
	return Vector4<T>(row(0).dot(vec), row(1).dot(vec), row(2).dot(vec), row(3).dot(vec));
}

template <typename T>
bool Matrix4x4<T>::operator==(const Matrix4x4<T>& rhs) const {
	return values == rhs.values;
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const Matrix4x4<T>& mat) {
	return os << mat.toString();
}

template <typename T>
bool Matrix4x4<T>::nearlyEquals(const Matrix4x4<T>& rhs, float tolerance) {
	for (int i = 0; i < 16; i++) {
		if (!maths::nearlyEquals(values[i], rhs.values[i], tolerance)) return false;
	}
	return true;
}

template <typename T>
Matrix4x4<T> Matrix4x4<T>::transpose() const {
	Matrix4x4<T> ret;
	for (int r = 0; r < 4; r++) {
		for (int c = 0; c < 4; c++) {
			ret(r, c) = (*this)(c, r);
		}
	}
	return ret;
}

template<typename T>
Matrix4x4<T> Matrix4x4<T>::inverse() const {
	Matrix4x4<T> tmp = *this;
	Matrix4x4<T> ret = Matrix4x4<T>::identity;

	int n = 4;

	for (int p = 0; p < n; p++) {
		T largestNum = std::abs(tmp(p, p));
		int largestRow = p;
		for (int r = p + 1; r < n; r++) {
			T test = std::abs(tmp(r, p));
			if (test > largestNum) {
				largestNum = test;
				largestRow = r;
			}
		}
		for (int i = 0; i < n; i++) {
			std::swap(tmp(p, i), tmp(largestRow, i));
			std::swap(ret(p, i), ret(largestRow, i));
		}

		T num = tmp(p, p);
		T mul = 1 / num;

		for (int i = 0; i < n; i++) {
			tmp(p, i) *= mul;
			ret(p, i) *= mul;
		}

		for (int r = 0; r < n; r++) {
			if (r == p) continue;
			T mul = tmp(r, p);
			for (int i = 0; i < n; i++) {
				tmp(r, i) -= mul * tmp(p, i);
				ret(r, i) -= mul * ret(p, i);
			}
		}
	}

	return ret;
}

template<typename T>
Matrix3x3<T> Matrix4x4<T>::getLinearPart() const {
	return Matrix3x3<T>(col(0).xyz, col(1).xyz, col(2).xyz);
}

template <typename T>
Vector4<T> Matrix4x4<T>::row(int row) const {
	return Vector4<T>((*this)(row, 0), (*this)(row, 1), (*this)(row, 2), (*this)(row, 3));
}

template <typename T>
Vector4<T> Matrix4x4<T>::col(int col) const {
	return Vector4<T>((*this)(0, col), (*this)(1, col), (*this)(2, col), (*this)(3, col));
}

template<typename T>
std::tuple<Vector3<T>, Matrix4x4<T>, Vector3<T>> Matrix4x4<T>::decompose() {
	std::array<Vector4<T>, 4> columns = { col(0), col(1), col(2), col(3) };

	Vector3<T>   translate = columns[3].xyz;

	Matrix4x4<T> rotate(columns[0].unit3(), columns[1].unit3(), columns[2].unit3(), Vector4<T>(0, 0, 0, 1));

	Vector3<T>   scale = Vector3<T>(columns[0].length3(), columns[1].length3(), columns[2].length3());

	return { translate, rotate, scale };
}

template <typename T>
constexpr Matrix4x4<T> Matrix4x4<T>::rotationYawPitchRoll(T yaw, T pitch, T roll) {
	return rotationAxisAngle(Vector3<T>::i, roll) * rotationAxisAngle(-Vector3<T>::j, pitch) * rotationAxisAngle(Vector3<T>::k, yaw);
}

template<typename T>
constexpr Matrix4x4<T> Matrix4x4<T>::rotationAxisAngle(Vector3<T> axis, T angle) {
	T c = std::cos(angle);
	T s = std::sin(angle);
	T t = 1 - c;
	Vector3<T> unitAxis;
	if (maths::nearlyEquals(axis.lengthSquared(), 1)) unitAxis = axis;
	else unitAxis = axis.unit();
	auto[x, y, z] = unitAxis;

	return Matrix4x4<T>(
		{ t * x * x + c    , t * x * y + z * s, t * x * z - y * s, 0,
		  t * x * y - z * s, t * y * y + c    , t * y * z + x * s, 0,
		  t * x * z + y * s, t * y * z - x * s, t * z * z + c    , 0,
		  0                , 0                , 0                , 1 }
	);
}

template<typename T>
Matrix4x4<T> Matrix4x4<T>::scale(Vector3<T> s) {
	return Matrix4x4<T>(
		{ s.x, 0  , 0  , 0,
		  0  , s.y, 0  , 0,
		  0  , 0  , s.z, 0,
		  0  , 0  , 0  , 1 }
	);
}

template<typename T>
Matrix4x4<T> Matrix4x4<T>::translate(Vector3<T> t) {
	return Matrix4x4<T>(
		{ 1  , 0  , 0  , 0,
		  0  , 1  , 0  , 0,
		  0  , 0  , 1  , 0,
		  t.x, t.y, t.z, 1 }
	);
}

template<typename T>
Matrix4x4<T> Matrix4x4<T>::trsYawPitchRoll(Vector3<T> scaleVec, T yaw, T pitch, T roll, Vector3<T> translateVec) {
	return translate(translateVec) * rotationYawPitchRoll(yaw, pitch, roll) * scale(scaleVec);
}

template<typename T>
Matrix4x4<T> Matrix4x4<T>::trsAxisAngle(Vector3<T> scaleVec, Vector3<T> axis, T angle, Vector3<T> translateVec) {
	return translate(translateVec) * rotationAxisAngle(axis, angle) * scale(scaleVec);
}

template<typename T>
Matrix4x4<T> Matrix4x4<T>::worldToView(T yaw, T pitch, T roll, Vector3<T> cameraPos) {
	return engineCoordinatesToGLWorldCoords * Matrix4x4<T>::rotationYawPitchRoll(-yaw, -pitch, -roll) * translate(-cameraPos);
}

template<typename T>
Matrix4x4<T> Matrix4x4<T>::worldToView(Matrix4x4<T> orientation, Vector3<T> cameraPos) {
	return engineCoordinatesToGLWorldCoords * orientation.transpose() * translate(-cameraPos);
}

template<typename T>
Matrix4x4<T> Matrix4x4<T>::worldToView(Quaternion<T> orientation, Vector3<T> cameraPos) {
	return engineCoordinatesToGLWorldCoords * orientation.conjugate().toMatrix() * translate(-cameraPos);
}

template<typename T>
Matrix4x4<T> Matrix4x4<T>::perspective(T fovy, T aspect, T zNear, T zFar) {
	T f = 1.0f / std::tan(fovy / 2.0f);
	Matrix4x4<T> ret;

	ret(0, 0) = f / aspect;
	ret(1, 1) = f;
	ret(2, 2) = (zFar + zNear) / (zNear - zFar);
	ret(2, 3) = (2.0f * zFar * zNear) / (zNear - zFar);
	ret(3, 2) = -1.0f;

	return ret;
}

// from http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/
template<typename T>
Quaternion<T> Matrix4x4<T>::toQuaternion() const {
	Quaternion<T> ret;
	T trace = (*this)(0, 0) + (*this)(1, 1) + (*this)(2, 2);
	if (trace > 0) {
		T s = std::sqrt(trace + 1) * 2;
		ret.w = s / 4;
		ret.x = ((*this)(2, 1) - (*this)(1, 2)) / s;
		ret.y = ((*this)(0, 2) - (*this)(2, 0)) / s;
		ret.z = ((*this)(1, 0) - (*this)(0, 1)) / s;
	}
	else if (((*this)(0, 0) > (*this)(1, 1)) && ((*this)(0, 0) > (*this)(2, 2))) {
		T s = std::sqrt(1 + (*this)(0, 0) - (*this)(1, 1) - (*this)(2, 2)) * 2;
		ret.w = ((*this)(2, 1) - (*this)(1, 2)) / s;
		ret.x = s / 4;
		ret.y = ((*this)(0, 1) + (*this)(1, 0)) / s;
		ret.z = ((*this)(0, 2) + (*this)(2, 0)) / s;
	}
	else if ((*this)(1, 1) > (*this)(2, 2)) {
		T s = sqrt(1 + (*this)(1, 1) - (*this)(0, 0) - (*this)(2, 2)) * 2;
		ret.w = ((*this)(0, 2) - (*this)(2, 0)) / s;
		ret.x = ((*this)(0, 1) + (*this)(1, 0)) / s;
		ret.y = s / 4;
		ret.z = ((*this)(1, 2) + (*this)(2, 1)) / s;
	}
	else {
		T s = sqrt(1 + (*this)(2, 2) - (*this)(0, 0) - (*this)(1, 1)) * 2;
		ret.w = ((*this)(1, 0) - (*this)(0, 1)) / s;
		ret.x = ((*this)(0, 2) + (*this)(2, 0)) / s;
		ret.y = ((*this)(1, 2) + (*this)(2, 1)) / s;
		ret.z = s / 4;
	}
	return ret;
}

template <typename T>
std::string Matrix4x4<T>::toString() const {
	std::stringstream ret;
	ret << std::fixed << std::setprecision(4) << "{ ";
	for (int r = 0; r < 4; r++) {
		for (int c = 0; c < 4; c++) {
			ret << std::setw(8) << (*this)(r, c);
			if (c < 3) ret << ", ";
			else if (r == 3 && c == 3) ret << " }";
			else ret << "\n  ";
		}
	}
	return ret.str();
}

namespace std {
	template <typename T>
	struct hash<Matrix4x4<T>> {
		std::size_t operator()(const Matrix4x4<T>& k) const {
			std::size_t curHash = 0;
			for (int i = 0; i < 16; i++) hashCombine(curHash, k.values[i]);
			return curHash;
		}
	};
}