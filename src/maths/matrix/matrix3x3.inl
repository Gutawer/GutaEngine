#pragma once
#include "hash.hpp"
#include "fmt/ostream.h"
#include <cmath>
#include <algorithm>
#include <string>
#include <iomanip>
#include <sstream>
#include <cassert>
#include "matrix3x3.hpp"

template <typename T>
const Matrix3x3<T> Matrix3x3<T>::identity = Matrix3x3<T>(
	{ 1, 0, 0,
	  0, 1, 0,
	  0, 0, 1 }
);

template <typename T>
Matrix3x3<T>::Matrix3x3(const Vector3<T>& v0, const Vector3<T>& v1, const Vector3<T>& v2) {
	(*this)(0, 0) = v0.x; (*this)(1, 0) = v0.y; (*this)(2, 0) = v0.z;
	(*this)(0, 1) = v1.x; (*this)(1, 1) = v1.y; (*this)(2, 1) = v1.z;
	(*this)(0, 2) = v2.x; (*this)(1, 2) = v2.y; (*this)(2, 2) = v2.z;
}

template <typename T>
inline T& Matrix3x3<T>::operator()(int row, int col) const {
	assert(row >= 0 && row <= 2 && col >= 0 && col <= 2);
	return values[col * 3 + row];
}

template <typename T>
Matrix3x3<T>& Matrix3x3<T>::operator+=(const Matrix3x3<T>& rhs) {
	for (int r = 0; r < 3; r++) {
		for (int c = 0; c < 3; c++) {
			(*this)(r, c) += rhs(r, c);
		}
	}
	return *this;
}

///
/// \relates Matrix3x3
/// 
/// \brief Adds two matrices together.
///
/// Adds two matrices together, adding each component.
///
/// \param lhs The first matrix to add
/// \param rhs The second matrix to add
///
/// \return A matrix equal to lhs + rhs
///
template <typename T>
Matrix3x3<T> operator+(Matrix3x3<T> lhs, const Matrix3x3<T>& rhs) {
	lhs += rhs;
	return lhs;
}

template <typename T>
Matrix3x3<T>& Matrix3x3<T>::operator-=(const Matrix3x3<T>& rhs) {
	for (int r = 0; r < 3; r++) {
		for (int c = 0; c < 3; c++) {
			(*this)(r, c) -= rhs(r, c);
		}
	}
	return *this;
}

///
/// \relates Matrix3x3
/// 
/// \brief Subtracts one matrix from another.
///
/// Subtracts one matrix from another, subtracting each component.
///
/// \param lhs The first matrix to subtract from
/// \param rhs The second matrix to subtract by
///
/// \return A matrix equal to lhs - rhs
///
template <typename T>
Matrix3x3<T> operator-(Matrix3x3<T> lhs, const Matrix3x3<T>& rhs) {
	lhs -= rhs;
	return lhs;
}

template <typename T>
Matrix3x3<T> Matrix3x3<T>::operator-() const {
	Matrix3x3<T> ret;
	for (int r = 0; r < 3; r++) {
		for (int c = 0; c < 3; c++) {
			ret(r, c) = -(*this)(r, c);
		}
	}
	return ret;
}

template <typename T>
Matrix3x3<T>& Matrix3x3<T>::operator*=(T rhs) {
	for (int i = 0; i < 9; i++) {
		values[i] *= rhs;
	}
	return *this;
}

///
/// \relates Matrix3x3
/// 
/// \brief Multiplies a matrix by a scalar.
///
/// Multiplies a matrix by a scalar, multiplying each component.
///
/// \param lhs The matrix to multiply
/// \param rhs The scalar to multiply by
///
/// \return A matrix equal to lhs * rhs
///
template <typename T>
Matrix3x3<T> operator*(Matrix3x3<T> lhs, T rhs) {
	lhs *= rhs;
	return lhs;
}

///
/// \relates Matrix3x3
/// 
/// \brief Multiplies a matrix by a scalar.
///
/// Multiplies a matrix by a scalar, multiplying each component.
///
/// \param lhs The scalar to multiply by
/// \param rhs The matrix to multiply
///
/// \return A matrix equal to lhs * rhs
///
template <typename T>
Matrix3x3<T> operator*(T lhs, const Matrix3x3<T>& rhs) {
	return rhs * lhs;
}

template <typename T>
Matrix3x3<T>& Matrix3x3<T>::operator*=(const Matrix3x3<T>& rhs) {
	Matrix3x3<T> temp;
	for (int r = 0; r < 3; r++) {
		for (int c = 0; c < 3; c++) {
			temp(r, c) = (*this)(r, 0) * rhs(0, c) + (*this)(r, 1) * rhs(1, c) + (*this)(r, 2) * rhs(2, c);
		}
	}
	*this = temp;
	return *this;
}

///
/// \relates Matrix3x3
///
/// \brief Multiplies a matrix by a matrix.
/// 
/// Multiplies a matrix by a matrix, performing transformation concatenation.
///
/// \param lhs The first matrix to multiply
/// \param rhs The second matrix to multiply
///
/// \return A matrix equal to lhs * rhs
///
template <typename T>
Matrix3x3<T> operator*(Matrix3x3<T> lhs, const Matrix3x3<T>& rhs) {
	lhs *= rhs;
	return lhs;
}

template <typename T>
Vector3<T> Matrix3x3<T>::operator*(const Vector3<T>& rhs) const {
	return Vector3<T>(row(0).dot(rhs), row(1).dot(rhs), row(2).dot(rhs));
}

template <typename T>
Vector3<T> Matrix3x3<T>::operator*(const Vector2<T>& rhs) const {
	Vector3<T> vec = Vector3<T>(rhs, 1);
	return Vector3<T>(row(0).dot(vec), row(1).dot(vec), row(2).dot(vec));
}

template <typename T>
bool Matrix3x3<T>::operator==(const Matrix3x3<T>& rhs) const {
	return values == rhs.values;
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const Matrix3x3<T>& mat) {
	return os << mat.toString();
}

template <typename T>
bool Matrix3x3<T>::nearlyEquals(const Matrix3x3<T>& rhs, float tolerance) {
	for (int i = 0; i < 9; i++) {
		if (!maths::nearlyEquals(values[i], rhs.values[i], tolerance)) return false;
	}
	return true;
}

template <typename T>
Matrix3x3<T> Matrix3x3<T>::transpose() const {
	Matrix3x3<T> ret;
	for (int r = 0; r < 3; r++) {
		for (int c = 0; c < 3; c++) {
			ret(r, c) = (*this)(c, r);
		}
	}
	return ret;
}

template<typename T>
Matrix3x3<T> Matrix3x3<T>::inverse() const {
	Matrix3x3<T> tmp = *this;
	Matrix3x3<T> ret = Matrix3x3<T>::identity;

	int n = 3;

	for (int p = 0; p < n; p++) {
		T largestNum = std::abs(tmp(p, p));
		int largestRow = p;
		for (int r = p + 1; r < n; r++) {
			T test = std::abs(tmp(r, p));
			if (test > largestNum) {
				largestNum = test;
				largestRow = r;
			}
		}
		for (int i = 0; i < n; i++) {
			std::swap(tmp(p, i), tmp(largestRow, i));
			std::swap(ret(p, i), ret(largestRow, i));
		}

		T num = tmp(p, p);
		T mul = 1 / num;

		for (int i = 0; i < n; i++) {
			tmp(p, i) *= mul;
			ret(p, i) *= mul;
		}

		for (int r = 0; r < n; r++) {
			if (r == p) continue;
			T mul = tmp(r, p);
			for (int i = 0; i < n; i++) {
				tmp(r, i) -= mul * tmp(p, i);
				ret(r, i) -= mul * ret(p, i);
			}
		}
	}

	return ret;
}

template <typename T>
Vector3<T> Matrix3x3<T>::row(int row) const {
	return Vector3<T>((*this)(row, 0), (*this)(row, 1), (*this)(row, 2));
}

template <typename T>
Vector3<T> Matrix3x3<T>::col(int col) const {
	return Vector3<T>((*this)(0, col), (*this)(1, col), (*this)(2, col));
}

template<typename T>
Matrix3x3<T> Matrix3x3<T>::rotate(T angle) {
	T cosAng = std::cos(angle);
	T sinAng = std::sin(angle);
	return Matrix3x3<T>(
		{  cosAng, sinAng, 0,
		  -sinAng, cosAng, 0,
		  0      , 0     , 1 }
	);
}

template<typename T>
Matrix3x3<T> Matrix3x3<T>::scale(Vector2<T> s) {
	return Matrix3x3<T>(
		{ s.x, 0  , 0,
		  0  , s.y, 0,
		  0  , 0  , 1 }
	);
}

template<typename T>
Matrix3x3<T> Matrix3x3<T>::translate(Vector2<T> t) {
	return Matrix3x3<T>(
		{ 1  , 0  , 0,
		  0  , 1  , 0,
		  t.x, t.y, 1 }
	);
}

template<typename T>
Matrix3x3<T> Matrix3x3<T>::trs(Vector2<T> scaleVec, T angle, Vector2<T> translateVec) {
	return translate(translateVec) * rotate(angle) * scale(scaleVec);
}

template <typename T>
std::string Matrix3x3<T>::toString() const {
	std::stringstream ret;
	ret << std::fixed << std::setprecision(4) << "{ ";
	for (int r = 0; r < 3; r++) {
		for (int c = 0; c < 3; c++) {
			ret << std::setw(8) << (*this)(r, c);
			if (c < 2) ret << ", ";
			else if (r == 2 && c == 2) ret << " }";
			else ret << "\n  ";
		}
	}
	return ret.str();
}

namespace std {
	template <typename T>
	struct hash<Matrix3x3<T>> {
		std::size_t operator()(const Matrix3x3<T>& k) const {
			std::size_t curHash = 0;
			for (int i = 0; i < 9; i++) hashCombine(curHash, k.values[i]);
			return curHash;
		}
	};
}
