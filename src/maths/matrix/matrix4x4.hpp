#pragma once
#include <array>
#include <tuple>
#include "maths/maths.hpp"

template <typename T>
class Vector3;

template <typename T>
class Vector4;

template <typename T>
class Quaternion;

/// A matrix class for 3D affine transformations.
template <typename T>
class Matrix4x4 {
public:
	/// The elements in this matrix.
	mutable std::array<T, 16> values{ 0 };

	/// The identity matrix.
	static const Matrix4x4<T> identity;

	/// Transforms from the coordinate system that the engine uses to the coordinate system traditionally used by perspective matrices in OpenGL.
	static const Matrix4x4<T> engineCoordinatesToGLWorldCoords;

	///
	/// \brief Creates a 0 matrix.
	///
	/// Creates a 0 matrix.
	///
	Matrix4x4<T>() : values{ 0 } {}

	///
	/// \brief Creates a matrix from an array of values.
	/// 
	/// Creates a matrix from an array of values, laid out in column-major order (like OpenGL).
	///
	/// \param arr The array to copy from.
	///
	Matrix4x4<T>(const std::array<T, 16>& arr) : values(arr) {}

	///
	/// \brief Creates a matrix from 4 vectors.
	///
	/// Creates a matrix from 4 vectors, where each column of the matrix is one of the vectors.
	///
	/// \param v0 The first column vector
	/// \param v1 The second column vector
	/// \param v2 The third column vector
	/// \param v3 The fourth column vector
	///
	Matrix4x4<T>(const Vector4<T>& v0, const Vector4<T>& v1, const Vector4<T>& v2, const Vector4<T>& v3);

	///
	/// \brief Accesses the value at a row and column of this matrix.
	///
	/// Accesses the value at a row and column of this matrix.
	///
	/// \param row The row to index from
	/// \param col The column to index from
	///
	/// \return The value at the row and column
	///
	inline T& operator()(int row, int col) const;

	///
	/// \brief Adds a matrix to this one.
	/// 
	/// Adds a matrix to this one, adding each element.
	///
	/// \param rhs The matrix to add
	///
	/// \return A reference to this matrix
	///
	Matrix4x4<T>& operator+=(const Matrix4x4<T>& rhs);

	///
	/// \brief Subtracts a matrix from this one.
	/// 
	/// Subtracts a matrix from this one, subtracting each element.
	///
	/// \param rhs The matrix to subtract
	///
	/// \return A reference to this matrix
	///
	Matrix4x4<T>& operator-=(const Matrix4x4<T>& rhs);

	///
	/// \brief Returns this matrix, but negated.
	/// 
	/// Returns this matrix, but with each element negated.
	///
	/// \return The negated matrix
	///
	Matrix4x4<T> operator-() const;

	///
	/// \brief Multiplies this matrix by a scalar.
	/// 
	/// Multiplies this matrix by a scalar, by multiplying each element.
	///
	/// \param rhs The scalar to multiply by
	///
	/// \return A reference to this matrix
	///
	Matrix4x4<T>& operator*=(T rhs);

	///
	/// \brief Multiplies this matrix by a matrix.
	/// 
	/// Multiplies this matrix by a matrix, performing transformation concatenation.
	///
	/// \param rhs The matrix to multiply by
	///
	/// \return A reference to this matrix
	///
	Matrix4x4<T>& operator*=(const Matrix4x4<T>& rhs);

	///
	/// \brief Multiplies this matrix by a 4D column vector.
	/// 
	/// Multiplies this matrix by a 4D column vector, applying a transformation.
	///
	/// \param rhs The vector to multiply by
	///
	/// \return The transformed vector
	///
	Vector4<T> operator*(const Vector4<T>& rhs) const;

	///
	/// \brief Multiplies this matrix by a 3D column vector.
	/// 
	/// Multiplies this matrix by a 3D column vector, turned internally into a 4D column vector with w component of 1, applying a transformation.
	///
	/// \param rhs The vector to multiply by
	///
	/// \return The transformed (4D) vector
	///
	Vector4<T> operator*(const Vector3<T>& rhs) const;

	///
	/// \brief Checks if two matrices are exactly equal.
	/// 
	/// Checks if two matrices are exactly equal, not accounting for floating point errors.
	///
	/// \param rhs The matrices to check equality for against this
	///
	/// \return Whether the matrices are exactly equal
	///
	bool operator==(const Matrix4x4<T>& rhs) const;

	///
	/// \brief Checks if two matrices are nearly equal.
	/// 
	/// Checks if two matrices are nearly equal, checking if each of their elements are within a certain tolerance.
	///
	/// \param rhs       The matrix to check near equality for against this
	/// \param tolerance The tolerance to allow for equality checks
	///
	/// \return Whether the matrices are nearly equal
	///
	bool nearlyEquals(const Matrix4x4<T>& rhs, float tolerance = maths::constants::kindaSmallNumber);

	///
	/// \brief Computes the transpose of this matrix.
	///
	/// Computes the transpose of this matrix.
	///
	/// \return The transpose of this matrix
	///
	Matrix4x4<T> transpose() const;

	///
	/// \brief Computes the inverse of this matrix.
	///
	/// Computes the inverse of this matrix.
	///
	/// \return The inverse of this matrix
	///
	Matrix4x4<T> inverse() const;

	Matrix3x3<T> getLinearPart() const;

	///
	/// \brief Returns a row of this matrix as a Vector3.
	///
	/// Returns a row of this matrix as a Vector3.
	///
	/// \param row The row to create a vector from.
	///
	/// \return The vector from row
	///
	Vector4<T> row(int row) const;

	///
	/// \brief Returns a column of this matrix as a Vector3.
	///
	/// Returns a column of this matrix as a Vector3.
	///
	/// \param col The column to create a vector from.
	///
	/// \return The vector from col
	///
	Vector4<T> col(int col) const;

	///
	/// \brief Decomposes this matrix into translation, rotation and scale.
	///
	/// Decomposes this matrix into translation, rotation and scale, where this = T * R * S.
	///
	/// \return A tuple containing the translation vector, the rotation matrix and scale vector, in that order.
	///
	std::tuple<Vector3<T>, Matrix4x4<T>, Vector3<T>> decompose();

	///
	/// \brief Creates a 3D rotation matrix from yaw, pitch and roll.
	///
	/// Creates a matrix representing a rotation by yaw around k, a rotation by pitch around i, and a rotation by roll around j.
	///
	/// \param yaw   The yaw, in radians
	/// \param pitch The pitch, in radians
	/// \param roll  The roll, in radians
	///
	/// \return The rotation matrix
	///
	constexpr static Matrix4x4<T> rotationYawPitchRoll(T yaw, T pitch, T roll);

	///
	/// \brief Creates a 3D rotation matrix from an axis and angle.
	///
	/// Creates a matrix representing a rotation by angle around axis.
	///
	/// \param axis  The axis to rotate around
	/// \param angle The angle, in radians
	///
	/// \return The rotation matrix
	///
	constexpr static Matrix4x4<T> rotationAxisAngle(Vector3<T> axis, T angle);

	///
	/// \brief Creates a 3D scale matrix.
	///
	/// Creates a matrix representing a 3D scale about the origin.
	///
	/// \param s The vector to scale by
	///
	/// \return The scale matrix
	///
	static Matrix4x4<T> scale(Vector3<T> s);

	///
	/// \brief Creates a 3D translation matrix.
	///
	/// Creates a matrix representing a 3D translation.
	///
	/// \param t The vector to translate by
	///
	/// \return The translate matrix
	///
	static Matrix4x4<T> translate(Vector3<T> t);

	///
	/// \brief Creates a 3D TRS matrix using yaw, pitch and roll.
	///
	/// Creates a matrix representing a 3D scale about the origin, followed by a 3D rotation by yaw, pitch and roll (see rotationYawPitchRoll()), followed by a 3D translation.
	///
	/// \param scaleVec     The vector to scale by
	/// \param yaw          The yaw, in radians
	/// \param pitch        The pitch, in radians
	/// \param roll         The roll, in radians
	/// \param translateVec The vector to translate by
	///
	/// \return The TRS matrix
	///
	static Matrix4x4<T> trsYawPitchRoll(Vector3<T> scaleVec, T yaw, T pitch, T roll, Vector3<T> translateVec);

	///
	/// \brief Creates a 3D TRS matrix using axis and angle.
	///
	/// Creates a matrix representing a 3D scale about the origin, followed by a 3D rotation by angle around axis (see rotationAxisAngle()), followed by a 3D translation.
	///
	/// \param scaleVec     The vector to scale by
	/// \param axis         The axis to rotate around
	/// \param angle        The angle to rotate by, in radians
	/// \param translateVec The vector to translate by
	///
	/// \return The TRS matrix
	///
	static Matrix4x4<T> trsAxisAngle(Vector3<T> scaleVec, Vector3<T> axis, T angle, Vector3<T> translateVec);

	///
	/// \brief Creates a world->view matrix from yaw, pitch and roll.
	///
	/// Creates a world->view matrix from yaw, pitch and roll, assuming that +X is right, +Y is forwards, +Z is upwards, and that yaw is counter-clockwise starting at +Y at 0.
	///
	/// \param yaw       The camera's yaw, in radians
	/// \param pitch     The camera's pitch, in radians
	/// \param roll      The camera's roll, in radians
	/// \param cameraPos The position of the camera
	///
	/// \return The world->view matrix.
	///
	static Matrix4x4<T> worldToView(T yaw, T pitch, T roll, Vector3<T> cameraPos);

	///
	/// \brief Creates a world->view matrix from a rotation matrix.
	///
	/// Creates a world->view matrix from a rotation matrix, assuming that +X is right, +Y is forwards, +Z is upwards, and that yaw is counter-clockwise starting at +Y at 0.
	///
	/// \param orientation The camera's orientation as a matrix
	/// \param cameraPos   The position of the camera
	///
	/// \return The world->view matrix.
	///
	static Matrix4x4<T> worldToView(Matrix4x4<T> orientation, Vector3<T> cameraPos);

	///
	/// \brief Creates a world->view matrix from a quaternion.
	///
	/// Creates a world->view matrix from a quaternion, assuming that +X is right, +Y is forwards, +Z is upwards.
	///
	/// \param orienation The camera's orientation as a quaternion
	/// \param cameraPos  The position of the camera
	///
	/// \return The world->view matrix.
	///
	static Matrix4x4<T> worldToView(Quaternion<T> orientation, Vector3<T> cameraPos);

	///
	/// \brief Creates a perspective projection matrix.
	///
	/// Creates a perspective projection matrix.
	///
	/// \param fovy   The vertical camera FOV
	/// \param aspect The aspect ratio of the screen
	/// \param zNear  The near clipping plane's distance
	/// \param zFar   The far clipping plane's distance
	///
	/// \return The perspective projection matrix
	///
	static Matrix4x4<T> perspective(T fovy, T aspect, T zNear, T zFar);

	///
	/// \brief Creates a quaternion from the top-left 3x3 matrix.
	///
	/// Creates a readable string from the top-left 3x3 matrix, representing its rotation.
	///
	/// \return The quaternion
	///
	Quaternion<T> toQuaternion() const;

	///
	/// \brief Creates a readable string from this matrix.
	///
	/// Creates a readable string from this matrix, in the form "{ 00, 01, 02, 03\n 10, ... 32, 33 }".
	///
	/// \return The readable string
	///
	std::string toString() const;
};

using Matrix4x4I = Matrix4x4<int>;
using Matrix4x4F = Matrix4x4<float>;
using Matrix4x4D = Matrix4x4<double>;

#include "matrix4x4.inl"