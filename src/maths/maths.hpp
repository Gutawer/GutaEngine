/// @file

#pragma once
#include <cmath>
#include <algorithm>
#include <xmmintrin.h>

namespace maths {
	namespace constants {
		static const float tau = 6.283185307179586476925286766559005768394338798750211641949f;
		static const float pi = 3.141592653589793238462643383279502884197169399375105820974f;
		static const float smallNumber = 1e-8f;
		static const float kindaSmallNumber = 1e-4f;
	}

	template <typename T>
	T smoothstep(T alpha) {
		alpha = std::clamp<T>(alpha, 0, 1);
		return (alpha * alpha * (3 - 2 * alpha));
	}

	template <typename T, typename U>
	T smoothstep(U alpha, T a, T b) {
		alpha = std::clamp<U>((alpha - a) / (b - a), 0, 1);
		return (alpha * alpha * (3 - 2 * alpha));
	}

	///
	/// \brief Linearly interpolates between a and b by alpha.
	///
	/// Linearly interpolates between a and b by alpha. Subject to floating point errors - see lerpStable() if the full numeric range of T is needed.
	///
	/// \param a     The starting value
	/// \param b     The ending value
	/// \param alpha How far along from a to b
	///
	/// \return The interpolated value
	///
	template <typename T, typename U>
	T lerp(T a, T b, U alpha) {
		return static_cast<T>(a + alpha * (b - a));
	}

	///
	/// \brief Linearly interpolates between a and b by alpha.
	///
	/// Linearly interpolates between a and b by alpha, handling the full numeric range of T unlike lerp().
	///
	/// \param a     The starting value
	/// \param b     The ending value
	/// \param alpha How far along from a to b
	///
	/// \return The interpolated value
	///
	template <typename T, typename U>
	T lerpStable(T a, T b, U alpha) {
		return static_cast<T>((1 - alpha) * a + alpha * b);
	}

	///
	/// \brief Checks if two numbers are nearly equal.
	///
	/// Checks if two floating point values are nearly equal with an epsilon check, to account for floating point errors when necessary.
	///
	/// \param n0        The first number to check
	/// \param n1        The second number to check
	/// \param tolerance The absolute tolerance to use
	///
	/// \return Whether the numbers are close enough to be considered equal
	///
	inline bool nearlyEquals(float n0, float n1, float tolerance = constants::smallNumber) {
		return std::abs(n1 - n0) <= tolerance;
	}

	///
	/// \brief Computes the inverse sqrt of a number.
	///
	/// Computes the inverse sqrt of a number.
	///
	/// \param f The number to calculate 1/sqrt of
	///
	/// \return The inverse sqrt of f
	///
	inline float invSqrt(float f) {
		const __m128 half = _mm_set_ss(0.5f);
		__m128 y0, x0, x1, x2, halfF;

		y0 = _mm_set_ss(f);
		x0 = _mm_rsqrt_ss(y0);
		halfF = _mm_mul_ss(y0, half);

		x1 = _mm_mul_ss(x0, x0);
		x1 = _mm_sub_ss(half, _mm_mul_ss(halfF, x1));
		x1 = _mm_add_ss(x0, _mm_mul_ss(x0, x1));

		x2 = _mm_mul_ss(x1, x1);
		x2 = _mm_sub_ss(half, _mm_mul_ss(halfF, x2));
		x2 = _mm_add_ss(x1, _mm_mul_ss(x1, x2));

		float answer = 0;
		_mm_store_ss(&answer, x2);
		return answer;
	}
}