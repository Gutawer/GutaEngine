#pragma once
#include <cmath>
#include <vector>
#include <utility>
#include <algorithm>

template <typename T>
class BezierPath {
public:
	template <typename U>
	static T cubicBezier(T p0, T p1, T p2, T p3, U t) {
		U tt  = t * t;
		U ttt = tt * t;
		U u   = 1 - t;
		U uu  = u * u;
		U uuu = uu * u;

		return uuu * p0 + 3 * uu * t * p1 + 3 * u * tt * p2 + ttt * p3;
	}

	template <typename U>
	static T cubicBezierDerivative(T p0, T p1, T p2, T p3, U t) {
		U tt = t * t;
		U u  = 1 - t;
		U uu = u * u;

		return 3 * uu * (p1 - p0) + 6 * u * t * (p2 - p1) + 3 * tt * (p3 - p2);
	}

	template <typename U>
	static T cubicBezierSecondDerivative(T p0, T p1, T p2, T p3, U t) {
		return 6 * (1 - t) * (p2 - 2 * p1 + p0) + 6 * t * (p3 - 2 * p2 + p1);
	}

	BezierPath(std::vector<T> initialPoints) {
		assert(((initialPoints.size() - 4) % 3) == 0);
		points = initialPoints;
	}

	void addPoints(std::array<T, 3> points) {
		this->points.insert(this->points.end(), points.begin(), points.end());
	}

	template <typename U>
	T get(U t) {
		if (points.size() >= 4) {
			t = std::clamp<U>(t, 0, 1);

			if (t == 1) return points.back();

			t *= (points.size() - 1) / 3;
			U fractT = 0;
			int intT = 0;

			U temp = 0;
			fractT = std::modf(t, &temp);
			intT = static_cast<int>(temp);
			int pointIndex = intT * 3;

			T p0 = points[pointIndex];
			T p1 = points[pointIndex + 1];
			T p2 = points[pointIndex + 2];
			T p3 = points[pointIndex + 3];

			return cubicBezier(p0, p1, p2, p3, fractT);
		}
		else return points[0];
	}

private:
	std::vector<T> points;
};