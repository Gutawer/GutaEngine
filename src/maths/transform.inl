#include "transform.hpp"
#pragma once

template <typename T>
Matrix4x4<T> Transform<T>::toMatrix() const {
	return Matrix4x4<T>::translate(translation) * orientation.toMatrix() * Matrix4x4<T>::scale(scale);
}

template <typename T>
Transform<T>& Transform<T>::operator*=(const Transform& rhs) {
	Transform ret;
	if (scale.x < 0 || scale.y < 0 || scale.z < 0 || rhs.scale.x < 0 || rhs.scale.y < 0 || rhs.scale.z < 0) {
		Matrix4x4<T> thisMatrix = toMatrix();
		Matrix4x4<T> rhsMatrix = rhs.toMatrix();

		Matrix4x4<T> concat = thisMatrix * rhsMatrix;

		auto [concatT, concatR, concatS] = concat.decompose();

		Vector3<T> newScale = Vector3<T>(scale.x * rhs.scale.x, scale.y * rhs.scale.y, scale.z * rhs.scale.z);
		Vector3<T> scaleSign = Vector3<T>(static_cast<T>(newScale.x >= 0 ? 1 : -1), static_cast<T>(newScale.y >= 0 ? 1 : -1), static_cast<T>(newScale.z >= 0 ? 1 : -1));

		Matrix4x4<T> temp = concatR * Matrix4x4<T>::scale(scaleSign);

		ret.orientation = temp.toQuaternion();
		ret.orientation = ret.orientation.unit();

		ret.scale = newScale;
		ret.translation = concatT;
	}
	else {
		ret.orientation = rhs.orientation * orientation;
		ret.scale = Vector3<T>(scale.x * rhs.scale.x, scale.y * rhs.scale.y, scale.z * rhs.scale.z);
		ret.translation = rhs.orientation * Vector3F(rhs.scale.x * translation.x, rhs.scale.y * translation.y, rhs.scale.z * translation.z) + rhs.translation;
	}
	*this = ret;
	return *this;
}

template <typename T>
Transform<T> operator*(Transform<T> lhs, const Transform<T>& rhs) {
	lhs *= rhs;
	return lhs;
}