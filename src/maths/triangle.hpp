#pragma once
#include "maths/vector/vector3.hpp"

template <typename T>
class Triangle3 {
public:
	Triangle3(Vector3<T> p0, Vector3<T> p1, Vector3<T> p2) : p0(p0), p1(p1), p2(p2) {}

	Vector3<T> p0;
	Vector3<T> p1;
	Vector3<T> p2;

	Vector3<T> getFaceNormal() {
		return (p2 - p0).cross(p1 - p0).unit();
	}

	T getArea() {
		return (p2 - p0).cross(p1 - p0).length() / 2;
	}
};

using Triangle3I = Triangle3<int>;
using Triangle3F = Triangle3<float>;
using Triangle3D = Triangle3<double>;