#pragma once
#include "maths/transform.hpp"
#include "world.hpp"
#include <set>
#include <vector>

class World;
class Game;
class ActorComponent;

class Actor {
	friend class Game; 
	friend class ActorComponent;
	friend class World;
public:
	Actor(std::shared_ptr<World> world) : world(world) {}
	~Actor();

	TransformF transform;

	void destroy();

	virtual void beginPlay() {}
	virtual void tick() {}
	virtual void onDestroy() {}

	std::shared_ptr<World> getWorldShared();
	std::weak_ptr<World> getWorldWeak();

	template<typename T, typename... Args>
	T* addComponent(Args... args);

	template<typename T>
	std::vector<T*> getComponents();
private:
	std::set<std::unique_ptr<ActorComponent>> components;

	std::weak_ptr<World> world;
};

template<typename T, typename... Args>
T* Actor::addComponent(Args... args) {
	auto iter = std::get<0>(components.insert(std::make_unique<T>(this, args...)));
	(*iter)->beginPlay();
	return dynamic_cast<T*>((*iter).get());
}

template<typename T>
std::vector<T*> Actor::getComponents() {
	std::vector<T*> ret;
	for (const auto& i : components) {
		if (dynamic_cast<T*>(i.get()) != nullptr) {
			ret.push_back(dynamic_cast<T*>(i.get()));
		}
	}
	return ret;
}