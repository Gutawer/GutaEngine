#pragma once
#include "graphics/graphics.hpp"
#include "window/window.hpp"
#include "input/input.hpp"
#include "player.hpp"
#include "world.hpp"
#include <stdexcept>
#include <string>
#include <memory>
#include <vector>
#include <memory>

/// General exception for game exceptions (primarily to be caught in main()).
class GameException : public std::runtime_error {
public:
	///
	/// \brief Creates a GameException.
	/// 
	/// Creates a GameException with a given message.
	///
	/// \param message The message to use
	///
	GameException(std::string msg) : std::runtime_error(msg) {}
};

/// The main game class.
class Game {
private:
	std::shared_ptr<graphics::Renderer> renderer;
	std::shared_ptr<window::WindowManager> window;

	const std::vector<std::string> args;

	std::shared_ptr<input::InputMapper> inputMapper;

	unsigned int gameTic = 0;

	std::shared_ptr<World> world;
public:
	///
	/// \brief Creates a game class.
	///
	/// Creates a game class with given command line arguments.
	///
	/// \param argc The number of arguments
	/// \param argv The arguments
	///
	Game(int argc, const char* argv[]);

	///
	/// \brief Gets the current tick the game is on.
	///
	/// Gets the current tick the game is on.
	///
	/// \return The current tick the game is on
	///
	unsigned int getGameTic() const;

	std::shared_ptr<World> getWorld();

	std::shared_ptr<World> getWorld() const;

	///
	/// \brief Runs the game loop.
	///
	/// Runs the game loop
	///
	void run();
};