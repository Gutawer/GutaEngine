#pragma once
#include "component.hpp"
#include "maths/transform.hpp"

class CameraComponent : public ActorComponent {
public:
	CameraComponent(Actor* owner) : ActorComponent(owner) {}

	TransformF additionalTransformation;

	float fov = 90.0f;
	float zNear = 5.0f;
	float zFar = 65535.0f;
};